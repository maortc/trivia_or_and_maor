import socket
SERVER_IP = '127.0.0.1'
SERVER_PORT = 7262
import json
def main():
    with socket.socket() as sock:
        server_address = (SERVER_IP, SERVER_PORT)
        sock.connect(server_address)

        server_msg = sock.recv(1024)
        server_msg = server_msg.decode()
        print(server_msg)

        if "Hello" in server_msg:
            choice = (input("1 - Login\nAny other key - Signup\n"))
            if choice == "1":
                username = input("Enter your username: ")
                password = input("Enter your password: ")

                myDict = {"username": username, "password" : password}

                bin = dict_to_binary(myDict)
                bin = "00110001 " + bin
                print(bin)

                sock.sendall((bin).encode())
                server_msg = sock.recv(1024)
                server_msg = server_msg.decode()
                print(server_msg)
            else:
                username = input("Enter your username: ")
                password = input("Enter your password: ")
                email = input("Enter your email: ")
                myDict = {"username" : username, "password": password, "email" : email}

                bin = dict_to_binary(myDict)
                bin = "00110010 " + bin


                sock.sendall((bin).encode())
                server_msg = sock.recv(1024)
                server_msg = server_msg.decode()
                print(server_msg)

def dict_to_binary(the_dict):
    str = json.dumps(the_dict)
    binary = ' '.join(format(ord(letter), 'b') for letter in str)
    return binary


def binary_to_dict(the_binary):
    jsn = ''.join(chr(int(x, 2)) for x in the_binary.split())
    d = json.loads(jsn)
    return d


if __name__ == "__main__":
    main()
