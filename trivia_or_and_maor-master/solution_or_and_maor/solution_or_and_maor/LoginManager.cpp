#include "LoginManager.h"

// Ctor
LoginManager::LoginManager(SqliteDataBase* db) : _dbHandle(db)
{

}

// Dtor
LoginManager::~LoginManager()
{
}

/*
* This function will signup the client
* Input: username, password, email
* Output: none
*/
void LoginManager::signup(std::string username, std::string password, std::string email)
{

	if (this->_dbHandle->doesUserExist(username)){
		throw std::exception("Error - username already exists");
	}
	this->_dbHandle->addUser(username, password, email);
	this->_loggedUsers.push_back(LoggedUser(username)); //adds the user to the active users list

}

/*
* This function will login the client
* Input: username, password
* Output: none
*/
void LoginManager::login(std::string username, std::string password)
{
	if (_dbHandle->isPasswordCorrect(username, password)) {//if the password is correct
		if (!isUserLoggedIn(username)) { //if the user isn't already logged in
			this->_loggedUsers.push_back(LoggedUser(username));//adds the user to the active users list
		}
		else{
			throw std::exception("Error - user was already logged in");
		}
	}
	else
	{
		throw std::exception("Error - incorrect username or password");
	}
	

}


/*
* This function will logout the client
* Input: username
* Output: none
*/
void LoginManager::logout(std::string username)
{
	auto toDelete = std::find(_loggedUsers.begin(), _loggedUsers.end(), username);//finds the pos of the user in _loggedUsers
	if (toDelete == _loggedUsers.end())
		throw std::exception("Error - user not found");
	_loggedUsers.erase(toDelete);

}

/*
* This function will check if the user is already logged in
* Input: username
* Output: True if he LoggedIn else false
*/
bool LoginManager::isUserLoggedIn(std::string username)
{
	return std::find(_loggedUsers.begin(), _loggedUsers.end(), LoggedUser(username)) != _loggedUsers.end();
}
