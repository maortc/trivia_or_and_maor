#include "communicator.h"
#include <iostream>
#include <bitset>
#include <mutex>
#include "loginRequestHandler.h"
#include <string>
#include <sstream>
#include <bitset>

using std::cout;
using std::endl;
std::mutex mtx;

//Ctor
Communicator::Communicator(RequestHandlerFactory& factory) : _handlerFactory(factory)
{
	_clientMap = new clientMap;
}

/*
* This function will for connect from the client and will add him to the client map
* Input: None
* Output: None
*/
void Communicator::startHandleRequests()
{
	bindAndListen();
	while (true) // while program run
	{
		SOCKET sock = accept(listeningSocket, NULL, NULL);

		//creating thread for user
		IRequestHandler* irh = _handlerFactory.createLoginRequestHandler();
		
		std::unique_lock<std::mutex> lock(mtx);
		_clientMap->insert({ sock, irh }); // Insert the login request to the clientMap
		lock.unlock();
		std::thread clientThread = std::thread(&Communicator::handleNewClient, this, sock);
		clientThread.detach(); //we want thread to run until client exit	

	}
}

/*
* This function will listen to connect from the client
* Input: None
* Output: None
*/
void Communicator::bindAndListen()//m
{
	//create listening socket
	sockaddr_in addr_in;
	listeningSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (listeningSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
	addr_in.sin_port = htons(PORT_NUM);
	addr_in.sin_family = AF_INET;
	addr_in.sin_addr.s_addr = INADDR_ANY;
	if (bind(listeningSocket, (struct sockaddr*)&addr_in, sizeof(addr_in)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	if (listen(listeningSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
}

/*
* This function will handle with new client and get all his messages
* Input: The socket of the client
* Output: None
*/
void Communicator::handleNewClient(SOCKET sock)
{
	int len;
	RequestInfo rqst;
	RequestResult res;
	std::string code = "";

	while (true)
	{
		
		char* buffer = getClientMsg(sock, len);

		if (buffer == nullptr) {
			return;
		}
		rqst.buffer = std::vector<std::bitset<8>>(len); //setting the request data type with the correct values
		for (int i = 0; i < len; i++) {
			rqst.buffer[i] = buffer[i];
		}

		rqst.receivalTime = std::time(0);

		for (int i = 0; i < 8; i++) // getting the first byte from the buffer (the code)
		{
			code += buffer[i];
		}

		// Convert the code brom bits to ascii
		std::stringstream sstream(code);
		std::string output = "";
		while (sstream.good())
		{
			std::bitset<8> bits;
			sstream >> bits;
			char c = char(bits.to_ulong());
			output += c;
		}


		rqst.id = std::stoi(output); // Convert the code from string to integer
		std::unique_lock<std::mutex> lock(mtx);

		res = _clientMap->find(sock)->second->handleRequest(rqst);
		delete _clientMap->find(sock)->second;
		_clientMap->find(sock)->second = res.newHandler;
		lock.unlock();


		code = "";
		try{
			sendMsg(sock, res);
		}
		catch (...)
		{
			return;
		}
		delete buffer;
	}

}

/*
* This function is helper method that get the message from the client
* Input: The socket of the client, The length of the message
* Output: The message of the client
*/
char* Communicator::getClientMsg(SOCKET clientSoc, int& lenBuf)
{
	char* buffer = new char[1024];
	lenBuf = recv(clientSoc, buffer, 1024, 0);
	if (INVALID_SOCKET == lenBuf) {
		closesocket(clientSoc);
		return nullptr;
	}
	if (lenBuf == 0) {
		return nullptr;
	}
	return buffer;
}

/*
* This function is helper method that send the message to the client
* Input: The socket of the client, the struct with all the info to send
* Output: None
*/
void Communicator::sendMsg(SOCKET clientSoc, RequestResult res)
{
	size_t lenBuf = res.buffer.size();
	char* toSend = new char[lenBuf];

	
	// Convert the buffer from bits to ascii
	for (int i = 5; i < lenBuf; i++) {
		toSend[i] = (char)res.buffer[i].to_ulong();
		cout << toSend[i];	
	}

	cout << endl;
	int lenBuffer = static_cast<int>(lenBuf); // Remove warning C4267
	if (send(clientSoc, toSend, lenBuffer, 0) == INVALID_SOCKET) {//sending the response to the client, checking if it fails
		closesocket(clientSoc);
		int err = WSAGetLastError();
		delete toSend;		
		throw std::exception((std::string("send failed with error ") + std::to_string(err)).c_str());
	}
	delete toSend;
}