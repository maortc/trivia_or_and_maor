#pragma once
#include <string>
#include <vector>
#include "LoggedUser.h"

struct RoomData {
	std::string name;
	unsigned int id;
	unsigned int questionsCount;
	unsigned int maxPlayers;
	unsigned int timePerQuestion;
	bool isActive;
};

class Room
{
public:
	Room(RoomData metadata);
	Room();
	~Room();
	void addUser(std::string username);
	void removeUser(std::string username);
	std::vector<std::string> getAllUsers();
	RoomData getMetadata();
	void setRoomState(bool newState);
	
private:
	RoomData m_metadata;
	
	std::vector<LoggedUser> m_users;

};

