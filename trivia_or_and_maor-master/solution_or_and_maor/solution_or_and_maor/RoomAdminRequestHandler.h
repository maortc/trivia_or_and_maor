#pragma once
#include "RequestHandlerFactory.h"
class RoomAdminRequestHandler: public IRequestHandler
{
public:
	RoomAdminRequestHandler(LoggedUser user, int id, RoomManager* roomManager, RequestHandlerFactory* handlerFactory);
	~RoomAdminRequestHandler();
	virtual bool isRequestRelevant(const RequestInfo& rqst) const override;
	virtual RequestResult handleRequest(const RequestInfo& rqst) override;

private:
	unsigned int m_roomID;
	LoggedUser m_user;
	RoomManager* m_roomManager;
	RequestHandlerFactory* m_handlerFactory;
	RequestResult closeRoom(RequestInfo rqst);
	RequestResult startGame(RequestInfo rqst);
	RequestResult getRoomState(RequestInfo rqst);
	void error(const char* errorMsg, RequestResult& toReturn);

};

