#pragma once
#include <map>
#include "Room.h"
class RoomManager
{
public:
	RoomManager();
	~RoomManager();
	void createRoom(LoggedUser admin, RoomData metadata);
	void deleteRoom(const int id);
	unsigned int getRoomState(int ID);
	std::vector<RoomData> getRooms();
	std::map<int, Room>& get_m_rooms();
	Room getRoomById(int roomId);
	bool roomExists(std::string name);
	bool roomExists(int roomId);
	void removePlayer(LoggedUser user, unsigned int id);
	void setRoomState(int ID, bool newState);
	


private:
	unsigned int m_nextRoomId;
	std::map<int, Room> m_rooms;
};

