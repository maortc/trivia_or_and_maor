#pragma once
#include "IRequestHandler.h"
#include "codes.h"
#include <string>
#include <iostream>
#include <string>
#include <vector>
#include <ctime>
#include <bitset>

struct LoginRequest
{
	std::string username;
	std::string password;
};

struct SignupRequest
{
	std::string username;
	std::string password;
	std::string email;
};

struct RequestInfo
{
	int id;
	std::time_t receivalTime;
	std::vector<std::bitset<8>> buffer;
};

struct RequestResult
{
	std::vector<std::bitset<8>> buffer;
	class IRequestHandler* newHandler;
};

struct JoinRoomRequest 
{
	unsigned int roomId;
};

struct GetPlayersInRoomRequest 
{
	unsigned int roomId;
};

struct CreateRoomRequest 
{
	std::string roomName;
	unsigned int maxUsers;
	unsigned int questionCount;
	unsigned int answerTimeout;
};


class IRequestHandler
{
public:
	virtual RequestResult handleRequest(const RequestInfo& rqst) = 0;
	virtual bool isRequestRelevant(const RequestInfo& rqst) const = 0;

};

