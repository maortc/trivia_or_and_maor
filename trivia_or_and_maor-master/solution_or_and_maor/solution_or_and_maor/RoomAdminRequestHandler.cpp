#include "RoomAdminRequestHandler.h"

// Ctor
RoomAdminRequestHandler::RoomAdminRequestHandler(LoggedUser user, int id, RoomManager* roomManager, RequestHandlerFactory* handlerFactory): m_roomManager(roomManager), m_handlerFactory(handlerFactory), m_user(user), m_roomID(id)
{}

// Dtor
RoomAdminRequestHandler::~RoomAdminRequestHandler()
{}

/*
* This function will check if the request is relevant
* Input: Request
* Output: True if relevant else false
*/
bool RoomAdminRequestHandler::isRequestRelevant(const RequestInfo& rqst) const
{
	if (rqst.id == MT_CLIENT_CLOSE_ROOM || rqst.id == MT_CLIENT_LEAVE_ROOM || rqst.id == MT_CLIENT_GET_ROOM_STATE)\
		return true;
	return false;
}

/*
* This function will check the request type and call to their function
* Input: RequestInfo
* Output: RequestResult
*/
RequestResult RoomAdminRequestHandler::handleRequest(const RequestInfo& rqst)
{
	switch (rqst.id) {
	case MT_CLIENT_CLOSE_ROOM || MT_CLIENT_LEAVE_ROOM:
		return closeRoom(rqst);
	case MT_CLIENT_START_GAME:
		return startGame(rqst);
	case MT_CLIENT_GET_ROOM_STATE:
		return getRoomState(rqst);
	default:
		RequestResult toReturn;
		toReturn.newHandler = nullptr;
		ErrorResponse errRes;
		errRes.message = "Error - request code not recognized";
		toReturn.buffer = JsonResponsePacketSerializer::serializeResponse(errRes);
		return toReturn;
	}

}

/*
* This function will close the room
* Input: RequestInfo
* Output: RequestResult
*/
RequestResult RoomAdminRequestHandler::closeRoom(RequestInfo rqst)
{
	RequestResult result;
	try {
		CloseRoomResponse response;
		m_roomManager->deleteRoom(m_roomID);
		result.newHandler = m_handlerFactory->createMenuRequestHandler(m_user.getUsername());
		response.status = true;
		result.buffer = JsonResponsePacketSerializer::serializeResponse(response);
	}
	catch (std::exception e) {
		error(e.what(), result);
	}
	return result;
}

/*
* This function will start the game
* Input: RequestInfo
* Output: RequestResult
*/
RequestResult RoomAdminRequestHandler::startGame(RequestInfo rqst)
{
	RequestResult result;
	try {
		StartGameResponse response;
		m_roomManager->setRoomState(m_roomID, true);
		result.newHandler = nullptr;
		response.status = true;
		result.buffer = JsonResponsePacketSerializer::serializeResponse(response);
		
	}
	catch (std::exception e) {
		error(e.what(), result);
	}
	return result;

}

/*
* This function will return the room state
* Input: RequestInfo
* Output: RequestResult
*/
RequestResult RoomAdminRequestHandler::getRoomState(RequestInfo rqst)
{
	RequestResult result;
	try {
		GetRoomStateResponse response;
		Room roomToGet = m_roomManager->getRoomById(m_roomID);
		RoomData metadata = roomToGet.getMetadata();
		
		result.newHandler = m_handlerFactory->createRoomAdminRequestHandler(m_user.getUsername(), m_roomID);

		response.status = true;
		response.answerTimeout = metadata.timePerQuestion;
		response.hasGameBegun = metadata.isActive;
		response.questionCount = metadata.questionsCount;
		response.players = roomToGet.getAllUsers();

		result.buffer = JsonResponsePacketSerializer::serializeResponse(response);
	}
	catch (std::exception e) {
		error(e.what(), result);
	}
	return result;

}

/*
* This function will handle the request if the request id is illegal
* Input: error message, RequestResult
* Output: None
*/
void RoomAdminRequestHandler::error(const char* errorMsg, RequestResult& toReturn)
{
	toReturn.newHandler = m_handlerFactory->createRoomAdminRequestHandler(m_user.getUsername(), m_roomID);
	ErrorResponse errRes =  { errorMsg };
	toReturn.buffer = JsonResponsePacketSerializer::serializeResponse(errRes);
}
