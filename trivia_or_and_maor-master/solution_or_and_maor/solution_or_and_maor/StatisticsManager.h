#pragma once
#include "IDatabase.h"
#include "SqliteDataBase.h"
#include <string>
#include <vector>
class StatisticsManager
{
public:
	StatisticsManager(SqliteDataBase* db);
	std::vector<std::string> getHighScore();
	std::vector<std::string> getUserStatistics(std::string username);

private:
	SqliteDataBase* m_database;
};

