#include "MenuRequestHandler.h"

// Ctor
MenuRequestHandler::MenuRequestHandler(RoomManager& roomManager, StatisticsManager& statisticsManager, std::string name, RequestHandlerFactory& handlerFactory) :
    m_handlerFactory(handlerFactory), m_roomManager(roomManager), m_statisticsManager(statisticsManager), m_user(name)
{}

/*
* This function will check if the request is legal
* Input: RequestInfo
* Output: True if legal else false
*/
bool MenuRequestHandler::isRequestRelevant(const RequestInfo & rqst) const
{
    return rqst.id == MT_CLIENT_LOG_OUT ||
		rqst.id == MT_CLIENT_GET_ROOMS||
		rqst.id == MT_CLIENT_GET_PLAYERS_IN_ROOM||
		rqst.id == MT_CLIENT_GET_HIGH_SCORE||
		rqst.id == MT_CLIENT_JOIN_ROOM ||
		rqst.id == MT_CLIENT_CREATE_ROOM;
}

/*
* This function will check the request type and call to their function
* Input: RequestInfo
* Output: RequestResult
*/ 
RequestResult MenuRequestHandler::handleRequest(const RequestInfo& rqst)
{
	switch (rqst.id)
	{
	case MT_CLIENT_LOG_OUT:
		return signout(rqst);
	case MT_CLIENT_GET_ROOMS:
		return getRooms(rqst);
	case MT_CLIENT_GET_PLAYERS_IN_ROOM:
		return getPlayersInRoom(rqst);
	case MT_CLIENT_GET_HIGH_SCORE:
		return getHighScore(rqst);
	case MT_CLIENT_JOIN_ROOM:
		return JoinRoom(rqst);
	case MT_CLIENT_CREATE_ROOM:
		return createRoom(rqst);
	default:
		RequestResult toReturn;
		toReturn.newHandler = nullptr;
		ErrorResponse errRes;
		errRes.message = "Error - request code not recognized";
		toReturn.buffer = JsonResponsePacketSerializer::serializeResponse(errRes);
		return toReturn;
	}
}

/*
* This function will signout the client
* Input: RequestInfo
* Output: RequestResult
*/
RequestResult MenuRequestHandler::signout(RequestInfo req)
{
	RequestResult result;
	LogoutResponse res;

	if (isRequestRelevant(req))
	{
		(m_handlerFactory.getLoginManager())->logout(m_user.getUsername());
		res.status = 1;
		result.newHandler = new loginRequestHandler();
	}
	else
	{
		res.status = 0;
		result.newHandler = nullptr;
	}

	result.buffer = JsonResponsePacketSerializer::serializeResponse(res);
	return result;

}

/*
* This function will return all the rooms
* Input: RequestInfo
* Output: RequestResult
*/
RequestResult MenuRequestHandler::getRooms(RequestInfo req)
{
	GetRoomResponse res;
	RequestResult result;

	if (isRequestRelevant(req))
	{
		res.rooms = this->m_roomManager.getRooms();		
		res.status = 1;
		result.newHandler =this->m_handlerFactory.createMenuRequestHandler(this->m_user.getUsername());
	}
	else
	{
		res.status = 0;
		result.newHandler = this->m_handlerFactory.createMenuRequestHandler(this->m_user.getUsername());
	}
	result.buffer = JsonResponsePacketSerializer::serializeResponse(res);
	return result;
}

/*
* This function will return all the players in the room
* Input: RequestInfo
* Output: RequestResult
*/
RequestResult MenuRequestHandler::getPlayersInRoom(RequestInfo req)
{
	RequestResult result;
	GetPlayersInRoomResponse response;
	GetPlayersInRoomRequest deserializedRequest = JsonRequestPacketDeserializer::deserializeGetPlayersRequest(req.buffer);
	if(isRequestRelevant(req))
	{ 
		response.players = m_roomManager.getRoomById(deserializedRequest.roomId).getAllUsers();
		result.newHandler = m_handlerFactory.createMenuRequestHandler(m_user.getUsername());
		result.buffer = JsonResponsePacketSerializer::serializeResponse(response);
	}
	else
	{
		result.newHandler = this->m_handlerFactory.createMenuRequestHandler(this->m_user.getUsername());
	}
	return result;
}

/*
* This function will return the high scores
* Input: RequestInfo
* Output: RequestResult
*/

RequestResult MenuRequestHandler::getHighScore(RequestInfo req)
{
	GetHighScoreResponse res;
	RequestResult result;
	
	if (isRequestRelevant(req))
	{
		res.statistics = this->m_statisticsManager.getHighScore();
		res.status = 1;
		result.newHandler = this->m_handlerFactory.createMenuRequestHandler(this->m_user.getUsername());;
	}
	else
	{
		res.status = 0;
		result.newHandler = this->m_handlerFactory.createMenuRequestHandler(this->m_user.getUsername());;
	}

	result.buffer = JsonResponsePacketSerializer::serializeResponse(res);
	return result;

}

/*
* This function will join the player into the room
* Input: RequestInfo
* Output: RequestResult
*/
RequestResult MenuRequestHandler::JoinRoom(RequestInfo req)
{
	RequestResult result;
	JoinRoomRequest joinReq;
	JoinRoomResponse res;

	try
	{
		if (isRequestRelevant(req))
		{
			joinReq = JsonRequestPacketDeserializer::deserializeJoinRoomRequest(req.buffer);
			if (this->m_roomManager.roomExists(joinReq.roomId))
			{
				this->m_roomManager.get_m_rooms()[joinReq.roomId].addUser(this->m_user.getUsername());
				res.status = 1;
				result.newHandler = this->m_handlerFactory.createRoomMemberRequestHandler(this->m_user.getUsername(), joinReq.roomId);;
			}
			else
			{
				throw "request invalid";
			}
		}
		else
		{
			throw "request invalid";
		}
	}
	catch (...)
	{
		res.status = 0;
		result.newHandler = this->m_handlerFactory.createMenuRequestHandler(this->m_user.getUsername());;
	}
	result.buffer = JsonResponsePacketSerializer::serializeResponse(res);
	return result;

}

/*
* This function will create a room
* Input: RequestInfo
* Output: RequestResult
*/
RequestResult MenuRequestHandler::createRoom(RequestInfo req)
{
	CreateRoomResponse res;
	RequestResult result;
	RoomData newData;
	CreateRoomRequest createReq;

	try
	{
		if (isRequestRelevant(req))
		{
			createReq = JsonRequestPacketDeserializer::deserializeCreateRoomReuquest(req.buffer);
			newData.name = createReq.roomName;
			newData.maxPlayers = createReq.maxUsers;
			newData.questionsCount = createReq.questionCount;
			newData.timePerQuestion = createReq.answerTimeout;
			newData.isActive = false;

			newData.id = (this->m_roomManager.getRooms().empty() ? 0 : this->m_roomManager.getRooms().rbegin()->id + 1);

			if (newData.questionsCount > 10 || newData.questionsCount < 0 ||
				this->m_roomManager.roomExists(newData.name))
			{
				throw "request invalid";
			}

			this->m_roomManager.createRoom(this->m_user, newData);

			res.status = 1;
			result.newHandler = this->m_handlerFactory.createRoomAdminRequestHandler(this->m_user.getUsername(),newData.id );
		}
		else
		{
			throw "request invalid";
		}
	}
	catch (...)
	{
		res.status = 0;
		result.newHandler = this->m_handlerFactory.createMenuRequestHandler(this->m_user.getUsername());
	}


	result.buffer = JsonResponsePacketSerializer::serializeResponse(res);
	return result;
}