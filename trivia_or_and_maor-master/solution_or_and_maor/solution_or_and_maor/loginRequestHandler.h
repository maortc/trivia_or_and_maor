#pragma once
#include "IRequestHandler.h"
#include "resStructs.h"
#include "JsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"
#include "RequestHandlerFactory.h"
#include "LoginManager.h"
#include "codes.h"
#include <iostream>
#include <string>
#include <map>
#include <algorithm>
#include <vector>
class RequestHandlerFactory;


class loginRequestHandler : public IRequestHandler
{
public:
	loginRequestHandler();
	loginRequestHandler(LoginManager* loginManager, RequestHandlerFactory* handlerFactory);
	~loginRequestHandler();
	virtual RequestResult handleRequest(const RequestInfo& rqst) override;
	virtual bool isRequestRelevant(const RequestInfo& rqst) const override;

	RequestResult login(const RequestInfo& requestInfo);
	RequestResult signup(const RequestInfo& requestInfo);
private:
	LoginManager* _loginManager;
	RequestHandlerFactory* _handlerFactory;

};