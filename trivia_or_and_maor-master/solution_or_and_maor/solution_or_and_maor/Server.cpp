#include "Server.h"
#include "WSAInitializer.h"
#include <thread>
#include <string>
#include <iostream>

//Ctor
Server::Server(SqliteDataBase* db) : _database(db),_handlerFactory(db)
{
}

/*
* This function will run the communicator and get the commands from the user
* Input: None
* Output: None
*/
void Server::run()
{
	com = new Communicator(this->_handlerFactory);
	WSAInitializer wsaInit;
	//creating thread for com
	std::thread communicatorThread = std::thread(&Communicator::startHandleRequests, com); 
	communicatorThread.detach();
	std::string command;
	do {
		std::cout << "Enter command:" << std::endl;
		std::cin >> command;
		if (command != "exit") 
		{
			std::cout << "Invalid input" << std::endl;
		}
	} while (command != "exit");
	std::cout << "GoodBye:)" << std::endl;

}
