#pragma once
#include <string>
class LoggedUser
{
public:
	LoggedUser(std::string username);
	~LoggedUser();
	std::string getUsername() const;
	bool operator==(const LoggedUser& other) const;
private:
	std::string _username;
};

