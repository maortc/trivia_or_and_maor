#pragma once
#include "communicator.h"

class Server
{
public:
	Server(SqliteDataBase* db);
	void run();
private:
	Communicator* com;
	SqliteDataBase* _database;
	RequestHandlerFactory _handlerFactory;
};

