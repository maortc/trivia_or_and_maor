#include "loginRequestHandler.h"

// Ctor
loginRequestHandler::loginRequestHandler()
{
}

// Ctor
loginRequestHandler::loginRequestHandler(LoginManager* loginManager, RequestHandlerFactory* handlerFactory) : _handlerFactory(handlerFactory), _loginManager(loginManager)
{
}

// Ctor
loginRequestHandler::~loginRequestHandler()
{
}

/*
* This function will login or signup the client
* Input: RequestInfo
* Ouput: RequestResult
*/
RequestResult loginRequestHandler::handleRequest(const RequestInfo& rqst)
{
    
    if (rqst.id == MT_CLIENT_LOG_IN)
    {
        return this->login(rqst);
    }
    else if (rqst.id == MT_CLIENT_SIGN_UP) 
    {
        return this->signup(rqst);
    }
    
    // If the code isnt login or signup
    RequestResult toReturn;
    toReturn.newHandler = this->_handlerFactory->createLoginRequestHandler();
    ErrorResponse errRes;
    errRes.message = "Error - request code not recognized";
    toReturn.buffer = JsonResponsePacketSerializer::serializeResponse(errRes);
    return toReturn;
}

/*
* This function will check if the request is relevant
* Input: Request
* Ouput: True if relevant else false
*/
bool loginRequestHandler::isRequestRelevant(const RequestInfo& rqst) const
{
    if (rqst.id == MT_CLIENT_LOG_IN || rqst.id == MT_CLIENT_SIGN_UP)
        return true;
    return false;
}

/*
* This function will login client
* Input: RequestInfo
* Ouput: RequestResult
*/
RequestResult loginRequestHandler::login(const RequestInfo& requestInfo)
{
    std::string username, password;
    RequestResult toReturn;
    LoginRequest translatedRqst = JsonRequestPacketDeserializer::deserializeLoginRequest(requestInfo.buffer);
    username = translatedRqst.username;
    password = translatedRqst.password;

    try
    {
        _loginManager->login(username, password);
        LoginResponse logRes;
        logRes.status = 1;
        toReturn.buffer = JsonResponsePacketSerializer::serializeResponse(logRes);
        toReturn.newHandler = this->_handlerFactory->createMenuRequestHandler(username);
    }
    catch (std::exception e) 
    {
        std::cout << e.what();
        std::cout << std::endl;
        LoginResponse logRes;
        std::string ex = e.what();
        if (ex == "Error - incorrect username or password")
        {
            logRes.status = 2;
        }
        else
        {
            logRes.status = 0;
        }
        
        toReturn.buffer = JsonResponsePacketSerializer::serializeResponse(logRes);
        toReturn.newHandler = this->_handlerFactory->createLoginRequestHandler();
    }
    return toReturn;
}

/*
* This function will signup client
* Input: RequestInfo
* Ouput: RequestResult
*/
RequestResult loginRequestHandler::signup(const RequestInfo& requestInfo)
{ 
    std::string username, password, email;
    RequestResult toReturn;
    SignupRequest translatedRqst = JsonRequestPacketDeserializer::deserializeSignupRequest(requestInfo.buffer);
    username = translatedRqst.username;
    password = translatedRqst.password;
    email = translatedRqst.email;

    try 
    {
        _loginManager->signup(username, password, email);
        SignupResponse signRes;
        signRes.status = 1;
        toReturn.buffer = JsonResponsePacketSerializer::serializeResponse(signRes);
        toReturn.newHandler = _handlerFactory->createMenuRequestHandler(username);
    }
    catch (std::exception e) {
        std::cout << e.what();
        std::cout << std::endl;
        SignupResponse signRes;
        signRes.status = 0;
        toReturn.buffer = JsonResponsePacketSerializer::serializeResponse(signRes);
        toReturn.newHandler = this->_handlerFactory->createLoginRequestHandler();
    }
    return toReturn;
}