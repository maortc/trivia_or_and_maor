#include "Room.h"

// Ctor
Room::Room(RoomData metadata) : m_metadata(metadata)
{
}

// Ctor
Room::Room()
{
}

// Dtor
Room::~Room()
{
}

/*
* This function will add user to the room
* Input: username
* Output: None
*/
void Room::addUser(std::string username)
{
	if (m_users.size() >= m_metadata.maxPlayers) {
		throw std::exception("Error - too many players");
	}
	m_users.push_back(LoggedUser(username));
}

/*
* This function will retomr user from the room
* Input: username
* Output: None
*/
void Room::removeUser(std::string username)
{
	auto user = std::find(m_users.begin(), m_users.end(), LoggedUser(username));
	if (user != m_users.end()) 
	{ 
		m_users.erase(user);
		return;
	}
	throw std::exception("Error - the user that was to be removed wasn't found");

}

/*
* This function will return all the users in the room
* Input: None
* Output: vector of all the usernames
*/
std::vector<std::string> Room::getAllUsers()
{
	std::vector<std::string> users(this->m_users.size());

	for (int i = 0; i < users.size(); i++)
	{
		users[i] = this->m_users[i].getUsername();
	}

	return users;

}

/*
* This function will return metadata
* Input: None
* Output: m_metadata
*/
RoomData Room::getMetadata()
{
	return m_metadata;
}
/*
* This function will set the room state
* Input: New state
* Output: None
*/
void Room::setRoomState(bool newState)
{
	m_metadata.isActive = newState;
}

