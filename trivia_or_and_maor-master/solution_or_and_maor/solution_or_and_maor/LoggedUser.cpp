#include "LoggedUser.h"

// Ctor
LoggedUser::LoggedUser(std::string username)
{
	this->_username = username;
}

// Dtor
LoggedUser::~LoggedUser()
{}

/*
* This function will return the username of the loogged usuer
* Input: None
* Output: The username
*/
std::string LoggedUser::getUsername() const
{
	return this->_username;
}

/*
* This function will check if there 2 logged user are equal
* Input: Other logged user
* Output: True if they equal else false
*/
bool LoggedUser::operator==(const LoggedUser& other) const
{
	return this->getUsername() == other.getUsername();
}
