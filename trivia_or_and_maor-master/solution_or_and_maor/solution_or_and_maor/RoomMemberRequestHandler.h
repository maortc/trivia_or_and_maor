#pragma once
#include "RequestHandlerFactory.h"
class RoomMemberRequestHandler : public IRequestHandler
{
public:
	RoomMemberRequestHandler(LoggedUser user, unsigned int id, RoomManager* roomManager, RequestHandlerFactory* handlerFactory);
	~RoomMemberRequestHandler();
	virtual bool isRequestRelevant(const RequestInfo& rqst) const override;
	virtual RequestResult handleRequest(const RequestInfo& rqst) override;


private:
	unsigned int m_roomID;
	LoggedUser m_user;
	RoomManager* m_roomManager;
	RequestHandlerFactory* m_handlerFactory;
	RequestResult leaveRoom(RequestInfo rqst);
	RequestResult getRoomState(RequestInfo rqst);
	void error(const char* errorMsg, RequestResult& toReturn);

};

