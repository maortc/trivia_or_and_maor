#include "JsonResponsePacketSerializer.h"
#include <bitset>

/*
* This function will serialize ErrorResponse
* Input: ErrorResponse
* Output: Vector of bits that contain the response
*/
std::vector<std::bitset<8>> JsonResponsePacketSerializer::serializeResponse(ErrorResponse error)
{
	std::map<std::string, std::string> typesValues;
	typesValues.insert({ "message", "\"" + error.message + "\"" });
	return JsonResponsePacketSerializer::serializeResponse(typesValues, MT_CLIENT_ERROR);
}

/*
* This function will serialize LoginResponse
* Input: LoginResponse
* Output: Vector of bits that contain the response
*/
std::vector<std::bitset<8>> JsonResponsePacketSerializer::serializeResponse(LoginResponse login)
{
	std::map<std::string, std::string> typesValues;
	typesValues.insert({ "status", std::to_string(login.status) });
	return serializeResponse(typesValues, MT_CLIENT_LOG_IN);
}

/*
* This function will serialize SignupResponse
* Input: SignupResponse
* Output: Vector of bits that contain the response
*/
std::vector<std::bitset<8>> JsonResponsePacketSerializer::serializeResponse(SignupResponse signup)
{
	std::map<std::string, std::string> typesValues;
	typesValues.insert({ "status", std::to_string(signup.status) });
	return serializeResponse(typesValues, MT_CLIENT_SIGN_UP);
}

/*
* This function will serialize LogoutResponse
* Input: LogoutResponse
* Output: Vector of bits that contain the response
*/
std::vector<std::bitset<8>> JsonResponsePacketSerializer::serializeResponse(LogoutResponse logout)
{
	std::map<std::string, std::string> typesValues;
	typesValues.insert({ "status", std::to_string(logout.status) });
	return serializeResponse(typesValues, MT_CLIENT_LOG_OUT);
}

/*
* This function will serialize GetRoomResponse
* Input: GetRoomResponse
* Output: Vector of bits that contain the response
*/
std::vector<std::bitset<8>> JsonResponsePacketSerializer::serializeResponse(GetRoomResponse room)
{
	std::map<std::string, std::string> typesValues;
	typesValues.insert({ "status", std::to_string(room.status) });
	typesValues.insert({ "Rooms", getRooms(room.rooms) });
	return serializeResponse(typesValues, MT_CLIENT_GET_ROOMS);
}

/*
* This function will serialize JoinRoomResponse
* Input: JoinRoomResponse
* Output: Vector of bits that contain the response
*/
std::vector<std::bitset<8>> JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse joinRoom)
{
	std::map<std::string, std::string> typesValues;
	typesValues.insert({ "status", std::to_string(joinRoom.status) });
	return serializeResponse(typesValues, MT_CLIENT_JOIN_ROOM);
}

/*
* This function will serialize CreateRoomResponse
* Input: CreateRoomResponse
* Output: Vector of bits that contain the response
*/
std::vector<std::bitset<8>> JsonResponsePacketSerializer::serializeResponse(CreateRoomResponse createRoom)
{
	std::map<std::string, std::string> typesValues;
	typesValues.insert({ "status", std::to_string(createRoom.status) });
	return serializeResponse(typesValues, MT_CLIENT_CREATE_ROOM);
}

/*
* This function will serialize GetHighScoreResponse
* Input: GetHighScoreResponse
* Output: Vector of bits that contain the response
*/
std::vector<std::bitset<8>> JsonResponsePacketSerializer::serializeResponse(GetHighScoreResponse highScore)
{
	std::map<std::string, std::string> typesValues;
	typesValues.insert({ "status", std::to_string(highScore.status) });
	typesValues.insert({ "high-score", getHighScore(highScore.statistics) });
	return serializeResponse(typesValues, MT_CLIENT_GET_HIGH_SCORE);

}

/*
* This function will serialize GetPlayersInRoomResponse
* Input: GetPlayersInRoomResponse
* Output: Vector of bits that contain the response
*/
std::vector<std::bitset<8>> JsonResponsePacketSerializer::serializeResponse(GetPlayersInRoomResponse response)
{
	std::map<std::string, std::string> typesValues;
	std::string players;
	for (std::vector<std::string>::const_iterator i = response.players.begin(); i != response.players.end(); ++i)
		players += *i;

	typesValues.insert({ "PlayersInRoom", players });
	return serializeResponse(typesValues, MT_CLIENT_GET_PLAYERS_IN_ROOM);
}

/*
* This function will serialize CloseRoomResponse
* Input: CloseRoomResponse
* Output: Vector of bits that contain the response
*/
std::vector<std::bitset<8>> JsonResponsePacketSerializer::serializeResponse(CloseRoomResponse response)
{
	std::map<std::string, std::string> typesValues;
	typesValues.insert({ "status", std::to_string(response.status) });
	return serializeResponse(typesValues, MT_CLIENT_CLOSE_ROOM);
}

/*
* This function will serialize StartGameResponse
* Input: StartGameResponse
* Output: Vector of bits that contain the response
*/
std::vector<std::bitset<8>> JsonResponsePacketSerializer::serializeResponse(StartGameResponse response)
{
	std::map<std::string, std::string> typesValues;
	typesValues.insert({ "status", std::to_string(response.status) });
	return serializeResponse(typesValues, MT_CLIENT_START_GAME);
}

/*
* This function will serialize LeaveRoomResponse
* Input: LeaveRoomResponse
* Output: Vector of bits that contain the response
*/
std::vector<std::bitset<8>> JsonResponsePacketSerializer::serializeResponse(LeaveRoomResponse response)
{
	std::map<std::string, std::string> typesValues;
	typesValues.insert({ "status", std::to_string(response.status) });
	return serializeResponse(typesValues, MT_CLIENT_LEAVE_ROOM);
}

/*
* This function will serialize GetRoomStateResponse
* Input: GetRoomStateResponse
* Output: Vector of bits that contain the response
*/
std::vector<std::bitset<8>> JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse response)
{
	std::map<std::string, std::string> typesValues;
	typesValues.insert({ "status", std::to_string(response.status) });
	typesValues.insert({ "answerTimeout", std::to_string(response.answerTimeout) });
	typesValues.insert({ "hasGameBegun", std::to_string(response.hasGameBegun) });
	typesValues.insert({ "players", makeVecToStr(response.players) });
	typesValues.insert({ "questionCount", std::to_string(response.questionCount) });
	return serializeResponse(typesValues, MT_CLIENT_GET_ROOM_STATE);

}

/*
* This function will serialize map of strings and the response type
* Input: map of strings, the response type
* Output: Vector of bits that contain the response
*/
std::vector<std::bitset<8>> JsonResponsePacketSerializer::serializeResponse(std::map<std::string, std::string> typesVals, int responseType)
{
	std::string strDump = createJsonDict(typesVals);
	std::vector<std::bitset<8>> toRet(strDump.length() + 1 + sizeof(int));
	char* msgLen = new char[sizeof(int)];
	int len = strDump.length();

	memcpy(msgLen, &len, sizeof(int));

	toRet[0] = responseType;
	for(unsigned int i = 1; i < 1 + sizeof(int); i++)
		toRet[i] = msgLen[i - 1];

	delete msgLen;

	for (unsigned int i = 0; i < strDump.length(); i++)
		toRet[i + 1 + sizeof(int)] = strDump[i];
	return toRet;
}

/*
* This function will parase maps of strings to json
* Input: map of string
* Output: String of the map in json
*/
std::string JsonResponsePacketSerializer::createJsonDict(std::map<std::string, std::string> typesVals)
{
	std::string toParse = "{";
	for (auto it = typesVals.begin(); it != typesVals.end(); it++) {
		if (it != typesVals.begin()) {
			toParse += ",";
		}
		toParse += "\"" + it->first + "\":" + it->second;
	}
	toParse += "}";
	return toParse;
}

/*
* This function will return all the rooms
* Input: Vector of RoomData
* Output: All the rooms names
*/
std::string JsonResponsePacketSerializer::getRooms(std::vector<RoomData> rooms)
{
	int i = 0;
	std::string message = "";

	for (i = 0; i < rooms.size(); i++)
	{
		message += rooms[i].name;
		if (i != rooms.size() - 1)
		{
			message += ", ";
		}
	}
	return message;

}

/*
* This function will return all the HighScores
* Input: Vector of string that containt the stats
* Output: All the high scores
*/
std::string JsonResponsePacketSerializer::getHighScore(std::vector<std::string> statistics)
{
	int i = 0;
	std::string message = "";
	
	for (i = 0; i < statistics.size(); i++)
	{
		message += statistics[i];
		if (i != statistics.size() - 1)
		{
			message += ", ";
		}
	}
	return message;

}

/*
* This function will turn vector of strings to one string
* Input: Vector of string
* Output: String with all the the strings of the vector
*/
std::string JsonResponsePacketSerializer::makeVecToStr(std::vector<std::string> vec)
{
	std::string toReturn = "[";
	for (auto it = vec.begin(); it != vec.end(); it++) {
		if (it != vec.begin()) {
			toReturn += ",";
		}
		toReturn += "\"" + (*it) + "\"";
	}
	toReturn += "]";
	return toReturn;
}
