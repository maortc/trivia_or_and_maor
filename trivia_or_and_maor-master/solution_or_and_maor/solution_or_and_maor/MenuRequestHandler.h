#pragma once
#include "IRequestHandler.h"
#include "LoginManager.h"
#include "RequestHandlerFactory.h"
#include "RoomManager.h"
#include "StatisticsManager.h"

class RequestHandlerFactory;


class MenuRequestHandler : public IRequestHandler
{
public:
	MenuRequestHandler(RoomManager& roomManager, StatisticsManager& statisticsManager, std::string name, RequestHandlerFactory& handlerFactory);
	virtual bool isRequestRelevant(const RequestInfo& rqst) const override;
	virtual RequestResult handleRequest(const RequestInfo& rqst) override;

private:
	LoggedUser m_user;
	RoomManager& m_roomManager;
	StatisticsManager& m_statisticsManager;
	RequestHandlerFactory& m_handlerFactory;

	RequestResult signout(RequestInfo);
	RequestResult getRooms(RequestInfo);
	RequestResult getPlayersInRoom(RequestInfo);
	RequestResult getHighScore(RequestInfo);
	RequestResult JoinRoom(RequestInfo);
	RequestResult createRoom(RequestInfo);

};

