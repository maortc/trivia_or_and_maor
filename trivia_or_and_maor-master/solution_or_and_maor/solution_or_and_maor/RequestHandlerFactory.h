#pragma once
#include "LoginManager.h"
#include "IDataBase.h"
#include "LoginRequestHandler.h"
#include "MenuRequestHandler.h"
#include "StatisticsManager.h"
#include "RoomManager.h"
#include "RoomMemberRequestHandler.h"
#include "RoomAdminRequestHandler.h"
 
class loginRequestHandler;
class MenuRequestHandler;
class RoomMemberRequestHandler;
class RoomAdminRequestHandler;

class RequestHandlerFactory
{
public:
	RequestHandlerFactory(SqliteDataBase* db);
	RequestHandlerFactory();
	~RequestHandlerFactory();
	loginRequestHandler* createLoginRequestHandler();
	MenuRequestHandler* createMenuRequestHandler(std::string username);
	StatisticsManager getStatisticsManager();
	RoomManager getRoomManager();
	LoginManager* getLoginManager();
	RoomMemberRequestHandler* createRoomMemberRequestHandler(std::string username, int id);
	RoomAdminRequestHandler* createRoomAdminRequestHandler(std::string username, int id);

private:
	LoginManager* _loginManager;
	SqliteDataBase* _dbHandle;
	StatisticsManager* m_StatisticsManager;
	RoomManager* m_roomManager;
};