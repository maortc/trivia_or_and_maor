﻿#include <iostream>
#include "Server.h"
#include "SqliteDataBase.h"
int main()
{
    SqliteDataBase* db = new SqliteDataBase();
    db->open();
    Server s(db);
    s.run();
 
}