#include "RoomManager.h"

// Ctor
RoomManager::RoomManager() : m_nextRoomId(0)
{
}

// Dtor
RoomManager::~RoomManager()
{
}

/*
* This funtion will create new room
* Input: admin, metadata
* Output: None
*/
void RoomManager::createRoom(LoggedUser admin, RoomData metadata)
{
	metadata.id = m_nextRoomId;
	Room room(metadata);
	room.addUser(admin.getUsername());
	m_rooms.insert({ m_nextRoomId, room });
	m_nextRoomId++;
}

/*
* This function will delete the remove
* Input: Room id
* Output: None
*/
void RoomManager::deleteRoom(const int id)
{
	if (m_rooms.find(id) == m_rooms.end()) {
		throw std::exception("Error - room doesn't exist");
	}
	m_rooms.erase(id);

}

/*
* This function will return the room state
* Input: Room id
* Output: Room state
*/
unsigned int RoomManager::getRoomState(int ID)
{
	return m_rooms[ID].getMetadata().isActive;
}

/*
* This function will return all the rooms
* Input: None
* Output: Vector that contain all the rooms
*/
std::vector<RoomData> RoomManager::getRooms()
{
	std::vector<RoomData> toReturn;
	for (auto it = m_rooms.begin(); it != m_rooms.end(); it++) {
		toReturn.push_back(it->second.getMetadata());
		
	}
	return toReturn;

}

/*
* This function will return the m_rooms
* Input: None
* Output: m_rooms
*/
std::map<int, Room>& RoomManager::get_m_rooms()
{
	return this->m_rooms;
}

/*
* This function will return room by his id
* Input: Room id
* Output: Room
*/
Room RoomManager::getRoomById(int roomId)
{
	auto room = m_rooms.find((unsigned int)roomId);
	if (room == m_rooms.end()) {
		throw std::exception("Error - room doesn't exist");
	}
	return room->second;

}

/*
* This function will check if room name exists
* Input: room name
* Output: True if exist else false
*/
bool RoomManager::roomExists(std::string name)
{
	for (auto& room : this->m_rooms)
	{
		if (room.second.getMetadata().name == name)
		{
			return true;
		}
	}
	return false;


}

/*
* This function will check if room id exists
* Input: room id
* Output: True if exist else false
*/
bool RoomManager::roomExists(int roomId)
{
	for (auto& room : this->m_rooms)
	{
		if (room.second.getMetadata().id == roomId)
		{
			return true;
		}
	}
	return false;
}

/*
* This function will remove player from the room
* Input: user, roomID
* Output: None
*/
void RoomManager::removePlayer(LoggedUser user, unsigned int id)
{
	if (m_rooms.find(id) == m_rooms.end()) {
		throw std::exception("Error - room doesn't exist");
	}
	m_rooms[id].removeUser(user.getUsername());

}

/*
* This function will set the room state
* Input: RoomId, new state
* Output: None
*/
void RoomManager::setRoomState(int ID, bool newState)
{
	m_rooms[ID].setRoomState(newState);
}

