#pragma once
#include <String>
class IDatabase
{
public:
	IDatabase();
	virtual void addUser(std::string username, std::string password, std::string email) = 0;

	// Querys
	virtual bool doesUserExist(std::string username) = 0;
	virtual bool isPasswordCorrect(std::string username, std::string password) = 0;
};