#pragma once

#include "codes.h"
#include "IRequestHandler.h"
#include <iostream>
#include <string>
#include <map>
#include <algorithm>
#include <vector>
#include <memory>
#include <bitset>
#include <stdio.h>
#include "json.hpp"

class JsonRequestPacketDeserializer
{
public:
	static LoginRequest deserializeLoginRequest(const std::vector<std::bitset<8>>& buffer);
	static SignupRequest deserializeSignupRequest(const std::vector<std::bitset<8>>& buffer);
	static GetPlayersInRoomRequest deserializeGetPlayersRequest(const std::vector<std::bitset<8>>& buffer);
	static JoinRoomRequest deserializeJoinRoomRequest(const std::vector<std::bitset<8>>& buffer);
	static CreateRoomRequest deserializeCreateRoomReuquest(const std::vector<std::bitset<8>>& buffer);


private:
	static std::string extractData(const std::vector<std::bitset<8>>& buffer);
	
};
std::vector<std::bitset<8>> slice(const std::vector<std::bitset<8>>& vec, const unsigned int start, const unsigned int len);