#include "JsonRequestPacketDeserializer.h"
#include <map>
#include <string>
#include <sstream>
#include <iostream>

using std::cout;
using std::endl;
/*
* This function will deserilaize login request
* Input: Vector of bytes
* Output: LoginRequest
*/
LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(const std::vector<std::bitset<8>>& buffer)
{
	LoginRequest request;
	std::string data = JsonRequestPacketDeserializer::extractData(buffer);
	std::string byte = "";
	std::string strJson = "";

	// Convert the data from bit to ascii
	for (unsigned int i = 0; i < buffer.size(); i++)
	{
		byte += (unsigned char)buffer[i].to_ulong();
		if ((unsigned char)buffer[i].to_ulong() == ' ')
		{
			byte = byte.substr(0, byte.size() - 1);
			std::bitset<8>a(byte);
			unsigned long j = a.to_ulong();
			unsigned char c = static_cast<unsigned char>(j);
			strJson += c;
			byte = "";
		}
	}

	strJson = strJson.substr(1, strJson.size()); // Remove the first char (the code)
	strJson += "}";
	
	nlohmann::json jsonObj = nlohmann::json::parse(strJson);
	
	std::string tempStr = std::string(jsonObj.value("username", "unknown"));
	request.username = tempStr;
	tempStr = std::string(jsonObj.value("password", "unknown"));
	request.password = tempStr;
	return request;
}
/*
* This function will deserilaize signup request
* Input: Vector of bytes
* Output: SignupRequest
*/
SignupRequest JsonRequestPacketDeserializer::deserializeSignupRequest(const std::vector<std::bitset<8>>& buffer)
{
	
	SignupRequest request;
	std::string data = JsonRequestPacketDeserializer::extractData(buffer);
	std::string byte = "";
	std::string strJson = "";

	// Convert the data from bit to ascii
	for (unsigned int i = 0; i < buffer.size(); i++)
	{
		byte += (unsigned char)buffer[i].to_ulong();
		if ((unsigned char)buffer[i].to_ulong() == ' ')
		{
			byte = byte.substr(0, byte.size() - 1);
			std::bitset<8>a(byte);
			unsigned long j = a.to_ulong();
			unsigned char c = static_cast<unsigned char>(j);
			strJson += c;
			byte = "";
		}
	}

	strJson = strJson.substr(1, strJson.size()); // Remove the first char (the code)
	strJson += "}";

	nlohmann::json jsonObj = nlohmann::json::parse(strJson);

	std::string tempStr = std::string(jsonObj.value("username", "unknown"));
	request.username = tempStr;
	tempStr = std::string(jsonObj.value("password", "unknown"));
	request.password = tempStr;
	tempStr = std::string(jsonObj.value("email", "unknown"));
	request.email = tempStr;
	return request;

}

/*
* This function will deserilaize playersInRoomRequest
* Input: Vector of bytes
* Output: PlayersInRoomRequest
*/
GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersRequest(const std::vector<std::bitset<8>>& buffer)
{
	GetPlayersInRoomRequest request;
	std::string data = JsonRequestPacketDeserializer::extractData(buffer);

	nlohmann::json jsonObj = nlohmann::json::parse(data);
	unsigned int tempStr = jsonObj.value("roomId", 0);
	request.roomId = tempStr;
	return request;
}

/*
* This function will deserilaize JoinRoomRequest
* Input: Vector of bytes
* Output: JoinRoomRequest
*/
JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(const std::vector<std::bitset<8>>& buffer)
{
	JoinRoomRequest request;
	std::string data = JsonRequestPacketDeserializer::extractData(buffer);
	std::string byte = "";
	std::string strJson = "";

	// Convert the data from bit to ascii
	for (unsigned int i = 0; i < buffer.size(); i++)
	{
		byte += (unsigned char)buffer[i].to_ulong();
		if ((unsigned char)buffer[i].to_ulong() == ' ')
		{
			byte = byte.substr(0, byte.size() - 1);
			std::bitset<8>a(byte);
			unsigned long j = a.to_ulong();
			unsigned char c = static_cast<unsigned char>(j);
			strJson += c;
			byte = "";
		}
	}

	strJson = strJson.substr(1, strJson.size()); // Remove the first char (the code)
	strJson += "}";
	nlohmann::json jsonObj = nlohmann::json::parse(strJson);
	unsigned int tempStr = jsonObj.value("roomId", 0);
	request.roomId = tempStr;
	return request;
}

/*
* This function will deserilaize CreateRoomRequest
* Input: Vector of bytes
* Output: CreateRoomRequest
*/
CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomReuquest(const std::vector<std::bitset<8>>& buffer)
{
	CreateRoomRequest request;
	std::string data = JsonRequestPacketDeserializer::extractData(buffer);
	std::string byte = "";
	std::string strJson = "";

	// Convert the data from bit to ascii
	for (unsigned int i = 0; i < buffer.size(); i++)
	{
		byte += (unsigned char)buffer[i].to_ulong();
		if ((unsigned char)buffer[i].to_ulong() == ' ')
		{
			byte = byte.substr(0, byte.size() - 1);
			std::bitset<8>a(byte);
			unsigned long j = a.to_ulong();
			unsigned char c = static_cast<unsigned char>(j);
			strJson += c;
			byte = "";
		}
	}

	strJson = strJson.substr(1, strJson.size()); // Remove the first char (the code)
	strJson += "}";

	nlohmann::json jsonObj = nlohmann::json::parse(strJson);
	std::string tempStr = std::string(jsonObj.value("roomName", "unknown"));
	request.roomName = tempStr;
	unsigned int tempNum = jsonObj.value("maxUsers", 0);
	request.maxUsers = tempNum;
	tempNum = jsonObj.value("questionCount", 0);
	request.questionCount = tempNum;
	tempNum = jsonObj.value("answerTimeout", 0);
	request.answerTimeout = tempNum;
	
	return request;

}

/*
* This function will return the data of the buffer
* Input: Buffer
* Output: The buffer with only the data
*/
std::string JsonRequestPacketDeserializer::extractData(const std::vector<std::bitset<8>>& buffer)
{
	unsigned int len = 0;
	std::vector<std::bitset<8>> sliced = slice(buffer, 1, 4);
	std::string toRet = "";
	memcpy(&len, sliced.data(), 4);
	sliced = slice(buffer, 1 + 4, len);
	for (unsigned int i = 0; i < len; i++) {
		toRet += (char)sliced[i].to_ulong();
	}
	return toRet;
}

/*
* This function will slice the vector of the bytes
* Input: Vector to slice, start index, len to slice
* Output: The sliced vector
*/
std::vector<std::bitset<8>> slice(const std::vector<std::bitset<8>>& vec, const unsigned int start, const unsigned int len)
{
	std::vector<std::bitset<8>> sliced(len);
	for (unsigned int i = 0; i < len; i++) {
		sliced[i] = (char)vec[i + start].to_ulong();
	}
	return sliced;
}