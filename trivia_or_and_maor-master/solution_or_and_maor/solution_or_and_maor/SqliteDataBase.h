#pragma once
#include "IDatabase.h"
#include "sqlite3.h"
#include <io.h>
#include <map>
#include <string>
#include <iostream>
#include <vector>
class SqliteDataBase : public IDatabase
{
public:
	SqliteDataBase();

	bool open();

	// Methods
	virtual void addUser(std::string username, std::string password, std::string email);

	// Querys
	virtual bool doesUserExist(std::string username);
	virtual bool isPasswordCorrect(std::string username, std::string password);

	// Stats
	double getPlayerAverageAnswerTime(std::string username);
	int getNumOfCorrectAnswers(std::string username);
	int getNumOfTotalAnswers(std::string username);
	int getNumOfPlayerGames(std::string username);
	std::vector<std::string> getTopFiveResults();

	// Callbacks
	static int playerAverageTimeCallback(void* data, int argc, char** argv, char** azColName);
	static int sumAnswersCallback(void* data, int argc, char** argv, char** azColName);
	static int playerGamesAnswersCallback(void* data, int argc, char** argv, char** azColName);
private:
	sqlite3* _dbHandle;
	std::vector<std::vector<std::string>> callbackUser(const char* command, std::vector<std::vector<std::string>>);
};
int callback(void* data, int argc, char** argv, char** azColName);