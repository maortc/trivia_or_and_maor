#pragma once
#pragma once
#pragma comment(lib, "ws2_32.lib")
#include <thread>
#include <map>
#include <string>
#include <WinSock2.h>
#include <Windows.h>
#include <exception>
#include "loginRequestHandler.h"
#include "IRequestHandler.h"

#define PORT_NUM 7262


using std::map;
class Communicator
{
public:
	Communicator(RequestHandlerFactory& factory);
	void startHandleRequests();
	
private:
	void bindAndListen();
	void handleNewClient(SOCKET sock);
	
	char* getClientMsg(SOCKET clientSoc, int& lenBuf);
	void sendMsg(SOCKET clientSoc, RequestResult res);

	SOCKET listeningSocket;
	RequestHandlerFactory& _handlerFactory;
	typedef map<SOCKET, IRequestHandler*> clientMap;
	clientMap* _clientMap;
};
