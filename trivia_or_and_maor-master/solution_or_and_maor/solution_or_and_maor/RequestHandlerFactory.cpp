#include "RequestHandlerFactory.h"

// Ctor
RequestHandlerFactory::RequestHandlerFactory(SqliteDataBase* db)
{
    this->_dbHandle = db;
    _loginManager = new LoginManager(this->_dbHandle);
    m_roomManager = new RoomManager();
    m_StatisticsManager = new StatisticsManager(db);
  
}

// Ctor
RequestHandlerFactory::RequestHandlerFactory()
{
}
// Dtor
RequestHandlerFactory::~RequestHandlerFactory()
{
    delete _loginManager;
    delete m_roomManager;
    delete m_StatisticsManager;
}

/*
* This function will create LoginRequestHandler
* Input: None
* Output: loginRequestHandler
*/
loginRequestHandler* RequestHandlerFactory::createLoginRequestHandler()
{
    return new loginRequestHandler(_loginManager, this);
}

/*
* This function will create MenuRequestHandler
* Input: username
* Output: MenuRequestHandler
*/
MenuRequestHandler* RequestHandlerFactory::createMenuRequestHandler(std::string username)
{
    return new MenuRequestHandler(*m_roomManager, *m_StatisticsManager, username, *this);
}

/*
* This function will return the statisticsManager
* Input: None
* Output: m_StatisticsManager
*/
StatisticsManager RequestHandlerFactory::getStatisticsManager()
{
    return *m_StatisticsManager;
}

/*
* This function will return the roomManager
* Input: None
* Output: m_roomManager
*/
RoomManager RequestHandlerFactory::getRoomManager()
{
    return *m_roomManager;
}

/*
* This function will return the loginManager
* Input: None
* Outpit: m_loginManager
*/
LoginManager* RequestHandlerFactory::getLoginManager()
{
    return this->_loginManager;
}

/*
* This function will create RoomMemberHandler
* Input: username, roomID
* Output: RoomMemberRequestHandler
*/
RoomMemberRequestHandler* RequestHandlerFactory::createRoomMemberRequestHandler(std::string username, int id)
{
    return new RoomMemberRequestHandler(username, id, m_roomManager, this);
}

/*
* This function will create RoomAdminHandler
* Input: username, roomID
* Output: RoomAdminRequestHandler
*/
RoomAdminRequestHandler* RequestHandlerFactory::createRoomAdminRequestHandler(std::string username, int id)
{
    return new RoomAdminRequestHandler(username, id, m_roomManager, this);
}
