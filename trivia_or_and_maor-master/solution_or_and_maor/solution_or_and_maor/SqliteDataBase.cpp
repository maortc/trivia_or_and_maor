#include "SqliteDataBase.h"
#include <string>
#include <io.h>
#include <algorithm>

// Ctor
SqliteDataBase::SqliteDataBase()
{}

/*
* This function will open the database
* Input: None
* Output: True if the database opened else false
*/
bool SqliteDataBase::open()
{
	sqlite3* db;
	std::string dbName = "triviaDB.sqlite";
	int doesFileExist = _access(dbName.c_str(), 0);
	int res = sqlite3_open(dbName.c_str(), &db);
	if (res != SQLITE_OK)
	{
		std::cout << "Error opening Database!" << std::endl;
		return false;
	}
	if (doesFileExist == 0)
	{
		
		this->_dbHandle = db;
		sqlite3_exec(_dbHandle, "PRAGMA foreign_keys = ON", nullptr, nullptr, nullptr);
		return true;
	}
	return false;
}

/*
* This function will add user to the database
* Input: username, password, email
* Output: None
*/
void SqliteDataBase::addUser(std::string username, std::string password, std::string email)
{
	char* errMessage;
	int res = sqlite3_exec(this->_dbHandle, (std::string("INSERT INTO User (username, password, email) VALUES ('") +
		username + "', '" + password + "', '" + email + "');").c_str()
		, nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK) {
		throw std::exception(errMessage);
	}

}

/*
* This function will check if the user exist
* Input: username
* Output: True if exist else false
*/
bool SqliteDataBase::doesUserExist(std::string username)
{
	std::vector < std::vector< std::string> > colNames;
	colNames.push_back(std::vector<std::string>());
	colNames[0].push_back("username"); //adding the column to search for
	return !callbackUser((std::string("SELECT username FROM User Where username = '") + username + "';").c_str(), colNames).empty();
}

/*
* This function will check if the password is correct
* Input: username, password
* Output: True if correct else false
*/
bool SqliteDataBase::isPasswordCorrect(std::string username, std::string password)
{
	std::vector < std::vector< std::string> > colNames;
	colNames.push_back(std::vector<std::string>());
	colNames[0].push_back("username");//adding the column to search for
	return !callbackUser((std::string("SELECT username FROM User WHERE username = '") + username + "' AND password = '" + password + "';").c_str(), colNames).empty();//if the returned vector isn't empty, meaning that the username was found
	
}

/*
* This function will return the avg answer time of the player
* Input: username
* Output: The average answer time
*/
double SqliteDataBase::getPlayerAverageAnswerTime(std::string username)
{
	double avg = 0;
	std::string sqlStatement;
	int res;
	char* errMessage;

	sqlStatement = "SELECT AVERAGEANSWERTIME FROM STATISTICS WHERE NAME == '" + username + "';";
	res = sqlite3_exec(this->_dbHandle, sqlStatement.c_str(), playerAverageTimeCallback, &avg, &errMessage);

	return avg;
}

/*
* This function will return how many correct answer the user have
* Input: username
* Output: Number of correct answers
*/
int SqliteDataBase::getNumOfCorrectAnswers(std::string username)
{
	int sum = 0;
	std::string sqlStatement;
	int res;
	char* errMessage;
	
	sqlStatement = "SELECT NUMOFCORRECTANSWERS FROM STATISTICS WHERE NAME == '" + username + "';";
	res = sqlite3_exec(this->_dbHandle, sqlStatement.c_str(), sumAnswersCallback, &sum, &errMessage);

	return sum;
}

/*
* This function will check how many answers the user have
* Input: username
* Output: Num of total answers
*/
int SqliteDataBase::getNumOfTotalAnswers(std::string username)
{
	int sum = 0;
	std::string sqlStatement;
	int res;
	char* errMessage;

	sqlStatement = "SELECT NUMOFTOTALANSWERS FROM STATISTICS WHERE NAME == '" + username + "';";
	res = sqlite3_exec(this->_dbHandle, sqlStatement.c_str(), sumAnswersCallback, &sum, &errMessage);

	return sum;
}

/*
* This function return the num of players games
* Input: username
* Output: Num of games
*/
int SqliteDataBase::getNumOfPlayerGames(std::string username)
{
	int games = 0;
	std::string sqlStatement;
	int res;
	char* errMessage;
	

	sqlStatement = "SELECT * FROM STATISTICS WHERE NAME == '" + username + "';";
	res = sqlite3_exec(this->_dbHandle, sqlStatement.c_str(), playerGamesAnswersCallback, &games, &errMessage);

	return games;

}

/*
* This function will return the top five best results
* Input: None
* Output: Vector of the five best results
*/
std::vector<std::string> SqliteDataBase::getTopFiveResults()
{
	std::vector<std::pair<int, std::string>> pairs;
	std::vector<std::string> vec;
	int counter = 0;

	std::string sqlStatement, current;
	int res;
	char* errMessage;
	
	sqlStatement = "SELECT PLAYERNAME, NUMOFCORRECTANSWERS FROM STATISTICS;";
	res = sqlite3_exec(this->_dbHandle, sqlStatement.c_str(), playerGamesAnswersCallback, &pairs, &errMessage);

	std::sort(vec.begin(), vec.end());
	for (int i = pairs.size() - 1; i >= 0 && counter != 5; i--)
	{
		current = pairs[i].second + " - " + std::to_string(pairs[i].first);
		counter++;
	}

	return vec;

}

// Callback
int SqliteDataBase::playerAverageTimeCallback(void* data, int argc, char** argv, char** azColName)
{
	std::vector<std::pair<int, std::string>>* vec = (std::vector<std::pair<int, std::string>>*)data;
	int score;
	std::string name;

	for (int i = 0; i < argc; i++)
	{
		if (azColName[i] == "PLAYERNAME")
		{
			score = std::atoi(argv[i]);
		}
		else if (azColName[i] == "NUMOFCORRECTANSWERS")
		{
			name = argv[i];
			vec->push_back(std::pair<int, std::string>(score, name));
		}
	}
	return 0;
}

// Callback
int SqliteDataBase::sumAnswersCallback(void* data, int argc, char** argv, char** azColName)
{
	int* sum = (int*)data;

	for (int i = 0; i < argc; i++)
	{
		*sum += std::atoi(argv[i]);
	}

	return 0;

}

// Callback
int SqliteDataBase::playerGamesAnswersCallback(void* data, int argc, char** argv, char** azColName)
{
	int* playergames = (int*)data;

	*playergames = argc;

	return 0;
}

// Callback
std::vector<std::vector<std::string>> SqliteDataBase::callbackUser(const char* command, std::vector<std::vector<std::string>> insertTo)
{
	char* errMessage;
	int res = sqlite3_exec(this->_dbHandle, command,
		callback, &insertTo, &errMessage);
	if (res != SQLITE_OK) {
		std::string str(errMessage);
		std::cout << str << std::endl;
		return insertTo;
	}
	insertTo.erase(insertTo.begin());//erasing the starting value that were added to specify which columns to get
	return insertTo;

}
// Callback
int callback(void* data, int argc, char** argv, char** azColName)
{
	std::vector<std::vector<std::string>>* cols = static_cast<std::vector<std::vector<std::string>>*>(data);
	int changed = 0, num_line = cols->size(), entered_in = 0;
	std::vector<std::string> remember = (*cols)[0];//remembering the column names to get
	cols->push_back(remember);
	for (int i = 0; i < argc; i++) {
		for (unsigned int j = 0; j < (*cols)[num_line].size(); j++) {//checking if the column name is equal to the one entered
			if ((*cols)[0][j].compare(azColName[i]) == 0) {
				if (argv[i] == NULL) {
					break;
				}
				(*cols)[num_line][j] = argv[i];//adding the argument
				changed++;
				entered_in++;
			}
		}
		if (entered_in == (*cols)[0].size()) {//if no arguments were entered into the vector
			entered_in = 0;
			num_line++;
			cols->push_back(remember);
		}
	}
	cols->pop_back();
	return 0;
}
