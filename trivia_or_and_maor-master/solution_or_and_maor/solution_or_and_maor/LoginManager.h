#pragma once
#include <vector>
#include <string>
#include "LoggedUser.h"
#include "SqliteDataBase.h"
#include <iostream>
class LoginManager
{
	SqliteDataBase* _dbHandle;
	std::vector<LoggedUser> _loggedUsers;

public:
	LoginManager(SqliteDataBase* db);
	~LoginManager();
	void signup(std::string username, std::string password, std::string email);
	void login(std::string username, std::string password);
	void logout(std::string username);
	bool isUserLoggedIn(std::string username);

};

