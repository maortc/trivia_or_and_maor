#pragma once
#include <iostream>
#include <string>
#include <map>
#include <algorithm>
#include <vector>
#include "codes.h"
#include "resStructs.h"
#include <bitset>
class JsonResponsePacketSerializer
{
public:
	// Serialize Response methods
	static std::vector<std::bitset<8>> serializeResponse(ErrorResponse response);
	static std::vector<std::bitset<8>> serializeResponse(LoginResponse response);
	static std::vector<std::bitset<8>> serializeResponse(SignupResponse response);
	static std::vector<std::bitset<8>> serializeResponse(LogoutResponse response);
	static std::vector<std::bitset<8>> serializeResponse(GetRoomResponse response);
	static std::vector<std::bitset<8>> serializeResponse(JoinRoomResponse response);
	static std::vector<std::bitset<8>> serializeResponse(CreateRoomResponse response);
	static std::vector<std::bitset<8>> serializeResponse(GetHighScoreResponse response);
	static std::vector<std::bitset<8>> serializeResponse(GetPlayersInRoomResponse response);
	static std::vector<std::bitset<8>> serializeResponse(CloseRoomResponse response);
	static std::vector<std::bitset<8>> serializeResponse(StartGameResponse response);
	static std::vector<std::bitset<8>> serializeResponse(LeaveRoomResponse response);
	static std::vector<std::bitset<8>> serializeResponse(GetRoomStateResponse response);

private:
	static std::vector<std::bitset<8>> serializeResponse(std::map<std::string , std::string> typesVals, int responseType);
	static std::string createJsonDict(std::map<std::string, std::string> typesVals);
	static std::string getRooms(std::vector<RoomData> rooms);
	static std::string getHighScore(std::vector<std::string> statistics);
	static std::string makeVecToStr(std::vector<std::string> vec);
};