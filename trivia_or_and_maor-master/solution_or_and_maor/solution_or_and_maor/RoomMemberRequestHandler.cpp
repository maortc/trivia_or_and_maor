#include "RoomMemberRequestHandler.h"

// Ctor
RoomMemberRequestHandler::RoomMemberRequestHandler(LoggedUser user, unsigned int id, RoomManager* roomManager, RequestHandlerFactory* handlerFactory) : m_handlerFactory(handlerFactory), m_roomManager(roomManager), m_roomID(id), m_user(user)
{}

// Dtor
RoomMemberRequestHandler::~RoomMemberRequestHandler()
{
}

/*
* This function will check if the request is relevant
* Input: Request
* Output: True if relevant else false
*/
bool RoomMemberRequestHandler::isRequestRelevant(const RequestInfo& rqst) const
{
    if (rqst.id == MT_CLIENT_LEAVE_ROOM || rqst.id == MT_CLIENT_GET_ROOM_STATE)
        return true;
    return false;
}

/*
* This function will check the request type and call to their function
* Input: RequestInfo
* Output: RequestResult
*/
RequestResult RoomMemberRequestHandler::handleRequest(const RequestInfo& rqst)
{
	switch (rqst.id) {
	case MT_CLIENT_LEAVE_ROOM:
		return leaveRoom(rqst);
	case MT_CLIENT_GET_ROOM_STATE:
		return getRoomState(rqst);
	default:
		RequestResult toReturn;
		toReturn.newHandler = nullptr;
		ErrorResponse errRes;
		errRes.message = "Error - request code not recognized";
		toReturn.buffer = JsonResponsePacketSerializer::serializeResponse(errRes);
		return toReturn;
	}
}

/*
* This function will remove the player from the room
* Input: RequestInfo
* Output: RequestResult
*/
RequestResult RoomMemberRequestHandler::leaveRoom(RequestInfo rqst)
{
	RequestResult result;
	
	try {
		CloseRoomResponse response;
		m_roomManager->removePlayer(m_user, m_roomID);
		result.newHandler = m_handlerFactory->createMenuRequestHandler(m_user.getUsername());
		response.status = true;
		result.buffer = JsonResponsePacketSerializer::serializeResponse(response);
			
	}
	catch (std::exception e) {
		error(e.what(), result);
	}
	return result;

}

/*
* This function will return the room state
* Input: RequestInfo
* Output: RequestResult
*/
RequestResult RoomMemberRequestHandler::getRoomState(RequestInfo rqst)
{
	RequestResult result;
	try {
		GetRoomStateResponse response;
		Room roomToGet = m_roomManager->getRoomById(m_roomID);
		RoomData metadata = roomToGet.getMetadata();
		
		result.newHandler = m_handlerFactory->createRoomMemberRequestHandler(m_user.getUsername(), m_roomID);

		response.status = true;
		response.answerTimeout = metadata.timePerQuestion;
		response.hasGameBegun = metadata.isActive;
		response.questionCount = metadata.questionsCount;
		response.players = roomToGet.getAllUsers();
		
		result.buffer = JsonResponsePacketSerializer::serializeResponse(response);
	}
	catch (std::exception e) {
		error(e.what(), result);
	}
	return result;

}

/*
* This function will handle the request if the request id is illegal
* Input: error message, RequestResult
* Output: None
*/
void RoomMemberRequestHandler::error(const char* errorMsg, RequestResult& toReturn)
{
	if (strcmp(errorMsg, "Error - room doesn't exist")) {
		toReturn.newHandler = m_handlerFactory->createMenuRequestHandler(m_user.getUsername());
	}
	else {
		toReturn.newHandler = m_handlerFactory->createRoomMemberRequestHandler(m_user.getUsername(), m_roomID);
	}

	ErrorResponse errRes = { errorMsg };
	toReturn.buffer = JsonResponsePacketSerializer::serializeResponse(errRes);

}
