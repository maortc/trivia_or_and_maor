#include "Helper.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <bitset>


using std::string;

// recieves the type code of the message from socket (3 bytes)
// and returns the code. if no message found in the socket returns 0 (which means the client disconnected)
int Helper::getMessageTypeCode(SOCKET sc)
{
	char* s = getPartFromSocket(sc, 3);
	std::string msg(s);

	if (msg == "")
		return 0;

	int res = std::atoi(s);
	delete s;
	return  res;
}


void Helper::send_update_message_to_client(SOCKET sc, const string& file_content, const string& second_username, const string &all_users)
{
	//TRACE("all users: %s\n", all_users.c_str())
	const string code = std::to_string(MT_SERVER_UPDATE);
	const string current_file_size = getPaddedNumber(file_content.size(), 5);
	const string username_size = getPaddedNumber(second_username.size(), 2);
	const string all_users_size = getPaddedNumber(all_users.size(), 5);
	const string res = code + current_file_size + file_content + username_size + second_username + all_users_size + all_users;
	//TRACE("message: %s\n", res.c_str());
	sendData(sc, res);
}

// recieve data from socket according byteSize
// returns the data as int
int Helper::getIntPartFromSocket(SOCKET sc, int bytesNum)
{
	char* s = getPartFromSocket(sc, bytesNum, 0);
	return atoi(s);
}

// recieve data from socket according byteSize
// returns the data as string
string Helper::getStringPartFromSocket(SOCKET sc, int bytesNum)
{
	char* s = getPartFromSocket(sc, bytesNum, 0);
	string res(s);
	return res;
}

// return string after padding zeros if necessary
string Helper::getPaddedNumber(int num, int digits)
{
	std::ostringstream ostr;
	ostr << std::setw(digits) << std::setfill('0') << num;
	return ostr.str();

}
std::string Helper::numToBin(int num)
{
	std::string binaryNumber = "";
	while (num > 0) {
		binaryNumber.insert(0, std::to_string(num % 2));
		num /= 2;
	}
	binaryNumber += "0";
	return binaryNumber;
}
std::string Helper::textToBin(std::string text)
{
	std::string binStrMsg = "";
	for (std::size_t i = 0; i < text.size(); ++i)
	{
		binStrMsg += std::bitset<8>(text.c_str()[i]).to_string();
	}
	return binStrMsg;
}
int Helper::getCodeBinary(std::string message)
{
	int binCode;
	int dec_value = 0;
	int base = 1;
	std::string strCode = "";
	
	for (int i = 0; i < 8; i++)
	{
		strCode += message[i];
	}

	
	binCode = stoi(strCode);
	
	int temp = binCode;
	while (temp >= 0) {
		int last_digit = temp % 10;
		temp = temp / 10;

		dec_value += last_digit * base;

		base = base * 2;
	}
	return dec_value;
}
int Helper::getLengthBinary(std::string message)
{
	int binLen;

	int dec_value = 0;
	int base = 1;
	std::string strLen = "";

	for (int i = 8; i < 40; i++)
	{
		strLen += message[i];
	}


	binLen = stoi(strLen);

	int temp = binLen;
	while (temp > 0) {
		int last_digit = temp % 10;
		temp = temp / 10;

		dec_value += last_digit * base;

		base = base * 2;
	}

	return dec_value;
}
std::string Helper::getMessageBinary(std::string message)
{
	int binLen = Helper::getLengthBinary(message);
	std::string strMsg = "";
	
	for (int i = 40; i < binLen; i++)
	{
		strMsg += message[i];
	}
	std::stringstream sstream(strMsg);
	std::string output;
	while (sstream.good())
	{
		std::bitset<8> bits;
		sstream >> bits;
		char c = char(bits.to_ulong());
		output += c;
	}

	return output;
}
// recieve data from socket according byteSize
// this is private function
char* Helper::getPartFromSocket(SOCKET sc, int bytesNum)
{
	return getPartFromSocket(sc, bytesNum, 0);
}

char* Helper::getPartFromSocket(SOCKET sc, int bytesNum, int flags)
{
	if (bytesNum == 0)
	{
		return (char*)"";
	}

	char* data = new char[bytesNum + 1];
	int res = recv(sc, data, bytesNum, flags);

	if (res == INVALID_SOCKET)
	{
		std::string s = "Error while recieving from socket: ";
		s += std::to_string(sc);
		throw std::exception(s.c_str());
	}

	data[bytesNum] = 0;
	return data;
}

// send data to socket
// this is private function
void Helper::sendData(SOCKET sc, std::string message)
{
	const char* data = message.c_str();

	if (send(sc, data, message.size(), 0) == INVALID_SOCKET)
	{
		throw std::exception("Error while sending message to client");
	}
}