#pragma once

#include <vector>
#include <string>
#include <WinSock2.h>


enum MessageType : byte
{
	MT_CLIENT_LOG_IN = 1,
	MT_CLIENT_SIGN_UP = 2,
	MT_CLIENT_LOG_OUT = 3,
	MT_CLIENT_ERROR = 4,
	MT_CLIENT_GET_ROOMS = 6,
	MT_CLIENT_GET_PLAYERS_IN_ROOM = 7,
	MT_CLIENT_JOIN_ROOM = 8,
	MT_CLIENT_CREATE_ROOM = 9,
	MT_CLIENT_GET_HIGH_SCORE = 10,
	MT_SERVER_UPDATE = 11
};


class Helper
{
public:


	static int getMessageTypeCode(SOCKET sc);
	static int getIntPartFromSocket(SOCKET sc, int bytesNum);
	static std::string getStringPartFromSocket(SOCKET sc, int bytesNum);
	static void sendData(SOCKET sc, std::string message);
	static void send_update_message_to_client(SOCKET sc, const std::string& file_content, const std::string& second_username, const std::string& all_users);
	static std::string getPaddedNumber(int num, int digits);
	
	static std::string numToBin(int num);
	static std::string textToBin(std::string text);
	
	static int getCodeBinary(std::string message);
	static int getLengthBinary(std::string message);
	static std::string getMessageBinary(std::string message);

private:
	static char* getPartFromSocket(SOCKET sc, int bytesNum);
	static char* getPartFromSocket(SOCKET sc, int bytesNum, int flags);

};


#ifdef _DEBUG // vs add this define in debug mode
#include <stdio.h>
// Q: why do we need traces ?
// A: traces are a nice and easy way to detect bugs without even debugging
// or to understand what happened in case we miss the bug in the first time
#define TRACE(msg, ...) printf(msg "\n", __VA_ARGS__);
// for convenient reasons we did the traces in stdout
// at general we would do this in the error stream like that
// #define TRACE(msg, ...) fprintf(stderr, msg "\n", __VA_ARGS__);

#else // we want nothing to be printed in release version
#define TRACE(msg, ...) printf(msg "\n", __VA_ARGS__);
#define TRACE(msg, ...) // do nothing
#endif