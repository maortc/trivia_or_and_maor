#include "StatisticsManager.h"

// Ctor 
StatisticsManager::StatisticsManager(SqliteDataBase* db) : m_database(db)
{
}

/*
* This function will reuturn the top five results
* Input: None
* Output: Vector that conatins the top five results
*/
std::vector<std::string> StatisticsManager::getHighScore()
{
	return m_database->getTopFiveResults();
}

/*
* This function will return the stats of the user
* Input: username
* Output: Vector that contains all the stats
*/
std::vector<std::string> StatisticsManager::getUserStatistics(std::string username)
{
	std::vector<std::string> stats;
	stats.push_back(std::to_string(this->m_database->getPlayerAverageAnswerTime(username)));
	stats.push_back(std::to_string(this->m_database->getNumOfCorrectAnswers(username)));
	stats.push_back(std::to_string(this->m_database->getNumOfTotalAnswers(username)));
	stats.push_back(std::to_string(this->m_database->getNumOfPlayerGames(username)));

	return stats;
}
