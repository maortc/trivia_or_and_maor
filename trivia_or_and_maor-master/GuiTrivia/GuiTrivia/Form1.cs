﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO.Compression;
using System.Text.Json;
using Newtonsoft.Json.Bson;
using NUnit.Framework.Internal.Execution;

namespace GuiTrivia
{


    public partial class Form1 : Form
    {
        TcpClient _client;
        private Form activeForm;

        byte[] _buffer = new byte[4096];
        public Form1()
        {
            InitializeComponent();
            _client = new TcpClient();
            this.FormClosing += Form1_FormClosing;
        }
        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);

            // Connect to the remote server. The IP address and port # could be
            // picked up from a settings file.
            _client.Connect("127.0.0.1", 7262);
            
            
        }


        private void OpenChildForm(Form childForm, object btnSender)
        {
            childForm.BringToFront();
            childForm.Show();
            childForm.FormClosed += new FormClosedEventHandler(Form_Closed);


        }



        void Form_Closed(object sender, FormClosedEventArgs e)
        {

            if (GlobalVars.isSignIn)
            {
                enableButtons();
            }


        }
        public void setIsSignIn(bool option)
        {
        }

        public void unVisibleButtons()
        {
            
            label3.Visible = false;
            button2.Visible = false;
            button6.Visible = false;
            button3.Visible = false;
            button5.Visible = false;
            
        }
        private void button1_Click(object sender, EventArgs e)
        {
            var temp = _client;
            OpenChildForm(new forms.Signin(temp), sender);
        }
        public void enableButtons()
        {
            button3.Enabled = true;
            button5.Enabled = true;
            button6.Enabled = false;
            button2.Enabled = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OpenChildForm(new forms.SignUp(_client), sender);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            var temp = _client;
            OpenChildForm(new forms.CreateRoom(temp), sender);
            //var json =
            //    @"9{""roomName"":""MyRoom"", ""maxUsers"":5, ""questionCount"":10,""answerTimeout"":0}";
            //string data = ToBinaryString(Encoding.UTF8, json);
            //var msg = Encoding.ASCII.GetBytes(data);
            //_client.GetStream().Write(msg, 0, msg.Length);


            //_client.GetStream().BeginRead(_buffer,
            //                                0,
            //                                _buffer.Length,
            //                                Server_MessageReceived,
            //                                null);
        }
        private string ToBinaryString(Encoding encoding, string text)
        {
            string s = string.Join("", encoding.GetBytes(text).Select(n => Convert.ToString(n, 2).PadLeft(8, '0')));

            return s
  .ToCharArray()
  .Aggregate("",
  (result, c) => result += ((!string.IsNullOrEmpty(result) && (result.Length + 1) % 9 == 0)
                            ? " " : "")
                           + c.ToString()
              );

        }
        private void Server_MessageReceived(IAsyncResult ar)
        {
            if (ar.IsCompleted)
            {

                // End the stream read
                var bytesIn = _client.GetStream().EndRead(ar);
                if (bytesIn > 0)
                {
                    // Create a string from the received data. For this server 
                    // our data is in the form of a simple string, but it could be
                    // binary data or a JSON object. Payload is your choice.
                    var tmp = new byte[bytesIn];
                    Array.Copy(_buffer, 0, tmp, 0, bytesIn);
                    string str = Encoding.ASCII.GetString(tmp);
                    BeginInvoke((Action)(() =>
                    {
                        str = str.Substring(5);

                        char status = getStatus(str);
                        if (status == '1')
                        {
                            MessageBox.Show("User logged in successfully!");
                            GlobalVars.isSignIn = true;
                            Close();

                        }
                        else if (status == '2')
                        {
                            MessageBox.Show("Error - incorrect username or password!");
                        }
                        else if (status == '0')
                        {
                            MessageBox.Show("The user is already logged in!");
                        }
                        else
                        {
                            MessageBox.Show("Unknown error!");
                        }
                    }));
                }
            }
        }
        private char getStatus(String str)
        {
            str = str = str.Substring(10);
            return str[0];
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Logout
            FormCollection fc = Application.OpenForms;
            bool bFormNameOpen = false;
            bool CreateRoomFormOpen = false;
            bool LobbyRoomAdminFormOpen = false;
            bool LobbyRoomMemberFormOpen = false;
            bool SignUpformOpen = false;
            bool JoinRoomformOpen = false;

            foreach (Form frm in fc)
            {
                //iterate through
                if (frm.Name == "Signin")
                {
                    bFormNameOpen = true;
                }
                if (frm.Name == "SignUp")
                {
                    SignUpformOpen = true;
                }
                if (frm.Name == "LoobyRoomAdmin")
                {
                    LobbyRoomAdminFormOpen = true;
                }
                if(frm.Name == "LobbyRoomMember")
                {
                    LobbyRoomMemberFormOpen = true;
                }
                if (frm.Name == "CreateRoom")
                {
                    CreateRoomFormOpen = true;
                }
                if (frm.Name == "JoinRoom")
                {
                    JoinRoomformOpen = true;
                }

            }
            if (e.CloseReason == CloseReason.UserClosing && !bFormNameOpen
                &&!SignUpformOpen && !LobbyRoomMemberFormOpen && !LobbyRoomAdminFormOpen && !CreateRoomFormOpen && !JoinRoomformOpen)
            { 
                var json = "3";

                string data = ToBinaryString(Encoding.UTF8, json);
                var msg = Encoding.ASCII.GetBytes(data);
                _client.GetStream().Write(msg, 0, msg.Length);
               

            }


            // Then assume that X has been clicked and act accordingly.
        }

        private void button3_Click(object sender, EventArgs e)
        {
            OpenChildForm(new forms.JoinRoom(_client), sender);
        }

    }
}