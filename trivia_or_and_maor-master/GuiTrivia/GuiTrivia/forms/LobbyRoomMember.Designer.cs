﻿namespace GuiTrivia.forms
{
    partial class LobbyRoomMember
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LobbyRoomMember));
            this.button70 = new System.Windows.Forms.Button();
            this.listBox70 = new System.Windows.Forms.ListBox();
            this.label74 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button70
            // 
            this.button70.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button70.Font = new System.Drawing.Font("Casual", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button70.ForeColor = System.Drawing.Color.DarkBlue;
            this.button70.Location = new System.Drawing.Point(272, 314);
            this.button70.Name = "button70";
            this.button70.Size = new System.Drawing.Size(193, 63);
            this.button70.TabIndex = 13;
            this.button70.Text = "Leave Room";
            this.button70.UseVisualStyleBackColor = true;
            this.button70.Click += new System.EventHandler(this.button70_Click);
            // 
            // listBox70
            // 
            this.listBox70.Font = new System.Drawing.Font("Aharoni", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.listBox70.FormattingEnabled = true;
            this.listBox70.ItemHeight = 15;
            this.listBox70.Location = new System.Drawing.Point(243, 214);
            this.listBox70.Name = "listBox70";
            this.listBox70.Size = new System.Drawing.Size(233, 94);
            this.listBox70.TabIndex = 12;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Casual", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.ForeColor = System.Drawing.Color.DarkBlue;
            this.label74.Location = new System.Drawing.Point(196, 169);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(321, 29);
            this.label74.TabIndex = 11;
            this.label74.Text = "Current participants are:";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Aharoni", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label73.ForeColor = System.Drawing.Color.DarkBlue;
            this.label73.Location = new System.Drawing.Point(549, 132);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(200, 20);
            this.label73.TabIndex = 10;
            this.label73.Text = "Time per question:0";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Aharoni", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label72.ForeColor = System.Drawing.Color.DarkBlue;
            this.label72.Location = new System.Drawing.Point(307, 132);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(230, 20);
            this.label72.TabIndex = 9;
            this.label72.Text = "Number of questions:0";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Aharoni", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label71.ForeColor = System.Drawing.Color.DarkBlue;
            this.label71.Location = new System.Drawing.Point(52, 132);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(233, 20);
            this.label71.TabIndex = 8;
            this.label71.Text = "Max number players:0";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Casual", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.ForeColor = System.Drawing.Color.DarkBlue;
            this.label70.Location = new System.Drawing.Point(111, 74);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(457, 42);
            this.label70.TabIndex = 7;
            this.label70.Text = "You are connect to room";
            // 
            // LobbyRoomMember
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button70);
            this.Controls.Add(this.listBox70);
            this.Controls.Add(this.label74);
            this.Controls.Add(this.label73);
            this.Controls.Add(this.label72);
            this.Controls.Add(this.label71);
            this.Controls.Add(this.label70);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LobbyRoomMember";
            this.Text = "Room";
            this.Load += new System.EventHandler(this.LobbyRoomMember_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button70;
        private System.Windows.Forms.ListBox listBox70;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label70;
    }
}