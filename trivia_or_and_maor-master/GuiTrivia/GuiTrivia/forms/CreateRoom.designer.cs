﻿namespace GuiTrivia.forms
{
    partial class CreateRoom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreateRoom));
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.button20 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox20
            // 
            this.textBox20.Location = new System.Drawing.Point(200, 29);
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(151, 20);
            this.textBox20.TabIndex = 0;
            // 
            // textBox21
            // 
            this.textBox21.Location = new System.Drawing.Point(200, 68);
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new System.Drawing.Size(151, 20);
            this.textBox21.TabIndex = 1;
            // 
            // textBox23
            // 
            this.textBox23.Location = new System.Drawing.Point(200, 139);
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new System.Drawing.Size(151, 20);
            this.textBox23.TabIndex = 3;
            // 
            // textBox22
            // 
            this.textBox22.Location = new System.Drawing.Point(200, 103);
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new System.Drawing.Size(151, 20);
            this.textBox22.TabIndex = 2;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Casual", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.DarkBlue;
            this.label20.Location = new System.Drawing.Point(12, 24);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(135, 25);
            this.label20.TabIndex = 4;
            this.label20.Text = "Room name";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Casual", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.DarkBlue;
            this.label21.Location = new System.Drawing.Point(12, 61);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(121, 25);
            this.label21.TabIndex = 5;
            this.label21.Text = "Max users";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Casual", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.DarkBlue;
            this.label22.Location = new System.Drawing.Point(12, 98);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(179, 25);
            this.label22.TabIndex = 6;
            this.label22.Text = "Question count";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Casual", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.DarkBlue;
            this.label23.Location = new System.Drawing.Point(12, 132);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(179, 25);
            this.label23.TabIndex = 7;
            this.label23.Text = "Answer timeout";
            // 
            // button20
            // 
            this.button20.Font = new System.Drawing.Font("Casual", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button20.ForeColor = System.Drawing.Color.DarkBlue;
            this.button20.Location = new System.Drawing.Point(100, 181);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(148, 53);
            this.button20.TabIndex = 8;
            this.button20.Text = "Create";
            this.button20.UseVisualStyleBackColor = true;
            this.button20.Click += new System.EventHandler(this.button20_Click);
            // 
            // CreateRoom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(370, 257);
            this.Controls.Add(this.button20);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.textBox23);
            this.Controls.Add(this.textBox22);
            this.Controls.Add(this.textBox21);
            this.Controls.Add(this.textBox20);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CreateRoom";
            this.Text = "Create Room";
            this.Load += new System.EventHandler(this.CreateRoom_Load);
            this.Controls.SetChildIndex(this.textBox20, 0);
            this.Controls.SetChildIndex(this.textBox21, 0);
            this.Controls.SetChildIndex(this.textBox22, 0);
            this.Controls.SetChildIndex(this.textBox23, 0);
            this.Controls.SetChildIndex(this.label20, 0);
            this.Controls.SetChildIndex(this.label21, 0);
            this.Controls.SetChildIndex(this.label22, 0);
            this.Controls.SetChildIndex(this.label23, 0);
            this.Controls.SetChildIndex(this.button20, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button button20;
    }
}