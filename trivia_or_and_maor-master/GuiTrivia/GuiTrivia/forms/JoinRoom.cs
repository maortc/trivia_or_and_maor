﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GuiTrivia.forms
{
    public partial class JoinRoom : Form1
    {
        TcpClient _client;
        byte[] _buffer = new byte[4096];
        string selectedRoom;
        public JoinRoom(TcpClient client)
        {
            InitializeComponent();
            _client = client;
            selectedRoom = "";
            base.unVisibleButtons();
        }
       
       
        private void Server_MessageReceived(IAsyncResult ar)
        {
            if (ar.IsCompleted)
            {
                // End the stream read
                var bytesIn = _client.GetStream().EndRead(ar);
                if (bytesIn > 0)
                {
                    // Create a string from the received data. For this server 
                    // our data is in the form of a simple string, but it could be
                    // binary data or a JSON object. Payload is your choice.
                    var tmp = new byte[bytesIn];
                    Array.Copy(_buffer, 0, tmp, 0, bytesIn);
                    string str = Encoding.ASCII.GetString(tmp);
                    BeginInvoke((Action)(() =>
                    {
                        
                        str = str.Substring(5);
                        char status = getStatus(str);
                        if (status == '1')
                        {
                            Hide();
                            Form childForm = new forms.LobbyRoomMember(_client);
                            childForm.BringToFront();
                            childForm.Show();

                            Close();
                            
                        }
                        else {
                           
                            
                             setRooms(str);
                                // Clear the buffer and start listening again
                               // Array.Clear(_buffer, 0, _buffer.Length);
                               // _client.GetStream().BeginRead(_buffer,
                                 //                               0,
                                   //                             _buffer.Length,
                                     //                           Server_MessageReceived,
                                       //                         null);
                            
                        }
                     
                       
                    }));
                }


            }
        }

        private void setRooms(string str)
        {
            str = getRoomsNames(str);
            GlobalVars.rooms.Clear();
            string roomName = "";
            listBox50.Items.Clear();
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] != ',')
                {
                    roomName += str[i];

                }
                else
                {
                    listBox50.Items.Add(roomName);
                    GlobalVars.rooms.Add(roomName, GlobalVars.RoomId.ToString());
                    GlobalVars.RoomId++;
                    roomName = "";
                    i++; // remove space


                }
               
            }
        }
        private string ToBinaryString(Encoding encoding, string text)
        {
            string s = string.Join("", encoding.GetBytes(text).Select(n => Convert.ToString(n, 2).PadLeft(8, '0')));

            return s
  .ToCharArray()
  .Aggregate("",
  (result, c) => result += ((!string.IsNullOrEmpty(result) && (result.Length + 1) % 9 == 0)
                            ? " " : "")
                           + c.ToString()
              );

        }

        private string getRoomsNames(string str)
        {
            string roomNames = "";
            
            for(int i = 9;i<str.Length;i++)
            {
                if(str[i] != '"')
                {
                    roomNames += str[i];
                    
                    
                    
                }
                else
                {
                    break;
                }
            }
            // remove last char
            //roomNames = roomNames.Remove(roomNames.Length - 1, 1);
            
            return roomNames;
        }
        private char getStatus(String str)
        {
            str = str.Substring(10);
            return str[0];
        }

        public void checkSelectedRoom()
        {
            selectedRoom = listBox50.SelectedItem.ToString();
        }

        private void checkState()
        {
            while (true)
            {
                string json = "6";

                string data = ToBinaryString(Encoding.UTF8, json);
                var msg = Encoding.ASCII.GetBytes(data);
                _client.GetStream().Write(msg, 0, msg.Length);


                _client.GetStream().BeginRead(_buffer,
                                                0,
                                                _buffer.Length,
                                                Server_MessageReceived,
                                                null);
                int milliseconds = 3000;
                Thread.Sleep(milliseconds);
            }
        }
        Thread t2;
        private void JoinRoom_Load(object sender, EventArgs e)
        {
            t2 = new Thread(checkState);
            t2.Start();
            t2.IsBackground = false;
        }

        private void button50_Click(object sender, EventArgs e)
        {
            checkSelectedRoom();
            if(selectedRoom != "")
            {
                string roomId;
                t2.Abort();

                GlobalVars.rooms.TryGetValue(selectedRoom, out roomId);
                string json = @"8{""roomId"":" + roomId;
                json += @"}";

                string data = ToBinaryString(Encoding.UTF8, json);
                var msg = Encoding.ASCII.GetBytes(data);
                _client.GetStream().Write(msg, 0, msg.Length);


                _client.GetStream().BeginRead(_buffer,
                                                0,
                                                _buffer.Length,
                                                Server_MessageReceived,
                                                null);
            }
            else
            {
                MessageBox.Show("You must select room!");
            }
            
        }
    }
}
