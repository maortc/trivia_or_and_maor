﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GuiTrivia.forms
{
    public partial class LobbyRoomMember : Form
    {
        TcpClient _client;
        byte[] _buffer = new byte[4096];
        int count;
        string roomClosedMsg;
        public LobbyRoomMember(TcpClient client)
        {
            InitializeComponent();
            _client = client;
            count = 0;
            roomClosedMsg = "{\"message\":\"Error - room doesn't exist\"}";
        }

        private string ToBinaryString(Encoding encoding, string text)
        {
            string s = string.Join("", encoding.GetBytes(text).Select(n => Convert.ToString(n, 2).PadLeft(8, '0')));

            return s
  .ToCharArray()
  .Aggregate("",
  (result, c) => result += ((!string.IsNullOrEmpty(result) && (result.Length + 1) % 9 == 0)
                            ? " " : "")
                           + c.ToString()
              );

        }
        private void Server_MessageReceived(IAsyncResult ar)
        {
            if (ar.IsCompleted)
            {
                // End the stream read
                var bytesIn = _client.GetStream().EndRead(ar);
                if (bytesIn > 0)
                {
                    // Create a string from the received data. For this server 
                    // our data is in the form of a simple string, but it could be
                    // binary data or a JSON object. Payload is your choice.
                    var tmp = new byte[bytesIn];
                    Array.Copy(_buffer, 0, tmp, 0, bytesIn);
                    string str = Encoding.ASCII.GetString(tmp);
                    BeginInvoke((Action)(() =>
                    {
                        str = str.Substring(5);
                        if(str == roomClosedMsg)
                        {
                            t1.Abort();
                            MessageBox.Show("Room closed!");
                            Hide();


                        }
                        else
                        {
                            setFiels(str);
                        }


                    }));
                }

                // Clear the buffer and start listening again
                Array.Clear(_buffer, 0, _buffer.Length);
                _client.GetStream().BeginRead(_buffer,
                                                0,
                                                _buffer.Length,
                                                Server_MessageReceived,
                                                null);

            }
        }
        private void setFiels(string str)
        {
            if(str.Length > 33)
            {
                if (str[34] == '1')
                {
                    if (count == 0)
                    {
                        MessageBox.Show("Game started!");
                        count++;
                    }
                }

            
            string ansTimeout = "";
            string players = "";
            for(int i = 17;i<str.Length;i++)
            {
                if(str[i] != ',')
                {
                    ansTimeout += str[i];
                }
                else
                {
                    label73.Text = "Time per question:" + ansTimeout;
                    break;
                }
            }
            label72.Text = "Number of questions:" + str[str.Length - 13];
            int flag = 0;
            for(int i = 0;i<str.Length;i++)
            {
                if (flag == 1)
                {
                    players += str[i];
                }

                if (str[i] == '[')
                {
                    flag = 1;
                }
                if(str[i] == ']')
                {
                    flag = 0;
                }
               
            }
            listBox70.Items.Clear();
            string roomName = "";
            for (int i = 0; i < players.Length-2; i++)
            {
                if (players[i] != ',')
                {
                    roomName += players[i];
                }
                else
                {
                    roomName = roomName.Trim('"');

                    listBox70.Items.Add(roomName);
                    roomName = "";
                    i++; // remove space
                }
            }
            roomName = roomName.Trim('"');
            listBox70.Items.Add(roomName);
            }
        }
        private void checkIfGameStarted(string str)
        {

        }
        Thread t1;
        private void LobbyRoomMember_Load(object sender, EventArgs e)
        {
            t1 = new Thread(checkState);
            t1.Start();
            t1.IsBackground = false;
        }

        private void checkState()
        {
            while (true)
            {
                string json = "4";

                string data = ToBinaryString(Encoding.UTF8, json);
                var msg = Encoding.ASCII.GetBytes(data);
                _client.GetStream().Write(msg, 0, msg.Length);


                _client.GetStream().BeginRead(_buffer,
                                                0,
                                                _buffer.Length,
                                                Server_MessageReceived,
                                                null);
                int milliseconds = 3000;
                Thread.Sleep(milliseconds);
            }
        }

        private void button70_Click(object sender, EventArgs e)
        {
            string json = @"3";
            t1.Abort();
            string data = ToBinaryString(Encoding.UTF8, json);
            var msg = Encoding.ASCII.GetBytes(data);
            _client.GetStream().Write(msg, 0, msg.Length);


            _client.GetStream().BeginRead(_buffer,
                                            0,
                                            _buffer.Length,
                                            Server_MessageReceived,
                                            null);
            Hide();
        }
    }
}
