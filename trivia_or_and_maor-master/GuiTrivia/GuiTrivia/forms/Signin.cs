﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GuiTrivia.forms
{
    public partial class Signin : Form1
    {
        TcpClient _client;
        byte[] _buffer = new byte[4096];
        public Signin(TcpClient client)
        {
            base.unVisibleButtons();
            InitializeComponent();
            _client = client;
        }

        private void Server_MessageReceived(IAsyncResult ar)
        {
            if (ar.IsCompleted)
            {
                // End the stream read
                var bytesIn = _client.GetStream().EndRead(ar);
                if (bytesIn > 0)
                {
                    // Create a string from the received data. For this server 
                    // our data is in the form of a simple string, but it could be
                    // binary data or a JSON object. Payload is your choice.
                    var tmp = new byte[bytesIn];
                    Array.Copy(_buffer, 0, tmp, 0, bytesIn);
                    string str = Encoding.ASCII.GetString(tmp);
                    _ = BeginInvoke((Action)(() =>
                      {
                          str = str.Substring(5);

                          char status = getStatus(str);
                          if (status == '1')
                          {
                              MessageBox.Show("User logged in successfully!");
                              GlobalVars.isSignIn = true;
                              //// Clear the buffer and start listening again
                              //Array.Clear(_buffer, 0, _buffer.Length);
                              //_client.GetStream().BeginRead(_buffer,
                              //                                0,
                              //                                _buffer.Length,
                              //                                Server_MessageReceived,
                              //                                null);
                              GlobalVars.LoggedUser = textBox1.Text;
                              Close();

                          }
                          else if (status == '2')
                          {
                              MessageBox.Show("Error - incorrect username or password!");
                          }
                          else if (status == '0')
                          {
                              MessageBox.Show("The user is already logged in!");
                          }
                          else
                          {
                              MessageBox.Show("Unknown error!");
                          }
                      }));
                }
                /*
                // Clear the buffer and start listening again
                Array.Clear(_buffer, 0, _buffer.Length);
                _client.GetStream().BeginRead(_buffer,
                                                0,
                                                _buffer.Length,
                                                Server_MessageReceived,
                                                null);
                */
            }
        }
        private string ToBinaryString(Encoding encoding, string text)
        {
            string s = string.Join("", encoding.GetBytes(text).Select(n => Convert.ToString(n, 2).PadLeft(8, '0')));

            return s
  .ToCharArray()
  .Aggregate("",
  (result, c) => result += ((!string.IsNullOrEmpty(result) && (result.Length + 1) % 9 == 0)
                            ? " " : "")
                           + c.ToString()
              );

        }
      
            private char getStatus(String str)
        {
            str = str = str.Substring(10);
            return str[0];
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if(isTextBoxsNotNulls())
            {
                string json =
               @"1{""username"":""" + textBox1.Text;
                json += @""",""password"":""" + textBox2.Text;
                json += @"""}";
                string data = ToBinaryString(Encoding.UTF8, json);
                var msg = Encoding.ASCII.GetBytes(data);
                _client.GetStream().Write(msg, 0, msg.Length);


                _client.GetStream().BeginRead(_buffer,
                                                0,
                                                _buffer.Length,
                                                Server_MessageReceived,
                                                null);
            }
            else
            {
                MessageBox.Show("You must fill all the fiels!");
            }
           

        }
        public bool isTextBoxsNotNulls()
        {
            if (textBox1.Text != "" && textBox2.Text != "")
                return true;
            return false;
        }

        private void Signin_Load(object sender, EventArgs e)
        {
            //// Clear the buffer and start listening again
            //Array.Clear(_buffer, 0, _buffer.Length);
            //_client.GetStream().BeginRead(_buffer,
            //                                0,
            //                                _buffer.Length,
            //                                Server_MessageReceived,
            //                                null);
        }
    }
}


