﻿namespace GuiTrivia.forms
{
    partial class JoinRoom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(JoinRoom));
            this.label50 = new System.Windows.Forms.Label();
            this.listBox50 = new System.Windows.Forms.ListBox();
            this.button50 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Casual", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.DarkBlue;
            this.label50.Location = new System.Drawing.Point(293, 29);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(177, 42);
            this.label50.TabIndex = 1;
            this.label50.Text = "Room list";
            // 
            // listBox50
            // 
            this.listBox50.Font = new System.Drawing.Font("Aharoni", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.listBox50.FormattingEnabled = true;
            this.listBox50.ItemHeight = 19;
            this.listBox50.Location = new System.Drawing.Point(215, 90);
            this.listBox50.Name = "listBox50";
            this.listBox50.Size = new System.Drawing.Size(348, 213);
            this.listBox50.TabIndex = 2;
            // 
            // button50
            // 
            this.button50.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button50.Font = new System.Drawing.Font("Casual", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button50.ForeColor = System.Drawing.Color.DarkBlue;
            this.button50.Location = new System.Drawing.Point(300, 326);
            this.button50.Name = "button50";
            this.button50.Size = new System.Drawing.Size(179, 71);
            this.button50.TabIndex = 3;
            this.button50.Text = "Join";
            this.button50.UseVisualStyleBackColor = true;
            this.button50.Click += new System.EventHandler(this.button50_Click);
            // 
            // JoinRoom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button50);
            this.Controls.Add(this.listBox50);
            this.Controls.Add(this.label50);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "JoinRoom";
            this.Text = "JoinRoom";
            this.Load += new System.EventHandler(this.JoinRoom_Load);
            this.Controls.SetChildIndex(this.label50, 0);
            this.Controls.SetChildIndex(this.listBox50, 0);
            this.Controls.SetChildIndex(this.button50, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.ListBox listBox50;
        private System.Windows.Forms.Button button50;
    }
}