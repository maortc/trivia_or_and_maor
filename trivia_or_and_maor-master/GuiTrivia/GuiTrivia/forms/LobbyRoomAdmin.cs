﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GuiTrivia.forms
{
    public partial class LobbyRoomAdmin : Form
    {
        TcpClient _client;
        byte[] _buffer = new byte[4096];
        public LobbyRoomAdmin(string roomName, string MaxNumberPlayers, string NumberOfQuestions, string TimePerQuestion, TcpClient client)
        {
            InitializeComponent();
            _client = client;
            label30.Text = "You are connected to room " + roomName;
            label31.Text = "Max number players:" + MaxNumberPlayers;
            label32.Text = "Number of questions:" + NumberOfQuestions;
            label33.Text = "Time per question:" + TimePerQuestion;
            listBox30.Items.Clear();
            listBox30.Items.Add(GlobalVars.LoggedUser);           

        }

        Thread t;
        private void LobbyRoomAdmin_Load(object sender, EventArgs e)
        {
            t = new Thread(checkState);
            t.Start();
            t.IsBackground = false;
        }
        private void Server_MessageReceived(IAsyncResult ar)
        {
            if (ar.IsCompleted)
            {
                // End the stream read
                var bytesIn = _client.GetStream().EndRead(ar);
                if (bytesIn > 0)
                {
                    // Create a string from the received data. For this server 
                    // our data is in the form of a simple string, but it could be
                    // binary data or a JSON object. Payload is your choice.
                    var tmp = new byte[bytesIn];
                    Array.Copy(_buffer, 0, tmp, 0, bytesIn);
                    string str = Encoding.ASCII.GetString(tmp);
                    BeginInvoke((Action)(() =>
                    {
                       str = str.Substring(5);
                        setFiels(str);
                                // Clear the buffer and start listening again
                                // Array.Clear(_buffer, 0, _buffer.Length);
                                // _client.GetStream().BeginRead(_buffer,
                                //                               0,
                                //                             _buffer.Length,
                                //                           Server_MessageReceived,
                                //                         null);
                            
                        


                    }));
                }


            }
        }

        private void setFiels(string str)
        {
            string players = "";
           
            int flag = 0;
            for (int i = 0; i < str.Length; i++)
            {
                if (flag == 1)
                {
                    players += str[i];
                }

                if (str[i] == '[')
                {
                    flag = 1;
                }
                if (str[i] == ']')
                {
                    flag = 0;
                }

            }

            string roomName = "";
            listBox30.Items.Clear();
            for (int i = 0; i < players.Length - 2; i++)
            {
                if (players[i] != ',')
                {
                    roomName += players[i];
                }
                else
                {
                    roomName = roomName.Trim('"');

                    listBox30.Items.Add(roomName);
                    roomName = "";
                    i++; // remove space
                }
            }
            roomName = roomName.Trim('"');
            listBox30.Items.Add(roomName);
        }
        private string ToBinaryString(Encoding encoding, string text)
        {
            string s = string.Join("", encoding.GetBytes(text).Select(n => Convert.ToString(n, 2).PadLeft(8, '0')));

            return s
  .ToCharArray()
  .Aggregate("",
  (result, c) => result += ((!string.IsNullOrEmpty(result) && (result.Length + 1) % 9 == 0)
                            ? " " : "")
                           + c.ToString()
              );

        }
        private void checkState()
        {
            while(true)
            {
                string json = "4";

                string data = ToBinaryString(Encoding.UTF8, json);
                var msg = Encoding.ASCII.GetBytes(data);
                _client.GetStream().Write(msg, 0, msg.Length);


                _client.GetStream().BeginRead(_buffer,
                                                0,
                                                _buffer.Length,
                                                Server_MessageReceived,
                                                null);
                int milliseconds = 3000;
                Thread.Sleep(milliseconds);
            }
        }

        private void button31_Click(object sender, EventArgs e)
        {
            string json = @"2";
            t.Abort();
            string data = ToBinaryString(Encoding.UTF8, json);
            var msg = Encoding.ASCII.GetBytes(data);
            _client.GetStream().Write(msg, 0, msg.Length);


            _client.GetStream().BeginRead(_buffer,
                                            0,
                                            _buffer.Length,
                                            Server_MessageReceived,
                                            null);
            MessageBox.Show("Game started!");
        }

        private void button30_Click(object sender, EventArgs e)
        {
            string json = @"1";
            t.Abort();
            string data = ToBinaryString(Encoding.UTF8, json);
            var msg = Encoding.ASCII.GetBytes(data);
            _client.GetStream().Write(msg, 0, msg.Length);


            _client.GetStream().BeginRead(_buffer,
                                            0,
                                            _buffer.Length,
                                            Server_MessageReceived,
                                            null);
            MessageBox.Show("Room closed!");
            Hide();
        }
    }
}

