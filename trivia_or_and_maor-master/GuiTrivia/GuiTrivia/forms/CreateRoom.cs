﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GuiTrivia.forms
{
    public partial class CreateRoom : Form1
    {
        TcpClient _client;
        byte[] _buffer = new byte[4096];
       
        public CreateRoom(TcpClient client)
        {
            InitializeComponent();
            base.unVisibleButtons();
            _client = client;
            
        }

        private void button20_Click(object sender, EventArgs e)
        {
            if(checkTextsBoxes())
            {
                string json =
                  @"9{""roomName"":""" + textBox20.Text;
                json += @""",""maxUsers"":" + textBox21.Text;
                json += @",""questionCount"":" + textBox22.Text;
                json += @",""answerTimeout"":" + textBox23.Text;
                json += @"}";
                string data = ToBinaryString(Encoding.UTF8, json);
                var msg = Encoding.ASCII.GetBytes(data);
                _client.GetStream().Write(msg, 0, msg.Length);


                _client.GetStream().BeginRead(_buffer,
                                                0,
                                                _buffer.Length,
                                                this.Server_MessageReceivedd,
                                                null);

            }
        }
        private string ToBinaryString(Encoding encoding, string text)
        {
            string s = string.Join("", encoding.GetBytes(text).Select(n => Convert.ToString(n, 2).PadLeft(8, '0')));

            return s
  .ToCharArray()
  .Aggregate("",
  (result, c) => result += ((!string.IsNullOrEmpty(result) && (result.Length + 1) % 9 == 0)
                            ? " " : "")
                           + c.ToString()
              );

        }
        private void Server_MessageReceivedd(IAsyncResult ar)
        {
            if (ar.IsCompleted)
            {
                // End the stream read
                var bytesIn = _client.GetStream().EndRead(ar);
                if (bytesIn > 0)
                {
                    // Create a string from the received data. For this server 
                    // our data is in the form of a simple string, but it could be
                    // binary data or a JSON object. Payload is your choice.
                    var tmp = new byte[bytesIn];
                    Array.Copy(_buffer, 0, tmp, 0, bytesIn);
                    string str = Encoding.ASCII.GetString(tmp);
                    BeginInvoke((Action)(() =>
                    {
                        str = str.Substring(5);

                        char status = getStatus(str);
                        if (status == '1')
                        {
                            MessageBox.Show("Room created successfully!");
                            GlobalVars.isSignIn = true;
                            
                            GlobalVars.rooms.Add(textBox20.Text, GlobalVars.RoomId.ToString());
                            GlobalVars.RoomId++;

                            Hide();
                            Form childForm = new forms.LobbyRoomAdmin(textBox20.Text, textBox21.Text, textBox22.Text, textBox23.Text, _client);
                            childForm.BringToFront();
                            childForm.Show();
                            Close();

                        }
                        
                        else
                        {
                            MessageBox.Show("Unknown error!");
                        }
                    }));
                }
            }
        }
        
            private char getStatus(String str)
        {
            str = str = str.Substring(10);
            return str[0];
        }

        private void CreateRoom_Load(object sender, EventArgs e)
        {
            // Clear the buffer and start listening again
         //   Array.Clear(_buffer, 0, _buffer.Length);
          //  _client.GetStream().BeginRead(_buffer,
           //                                 0,
             ///                               _buffer.Length,
               //                             Server_MessageReceivedd,
                //                            null);
        }
        private bool checkTextsBoxes()
        {

            if(textBox20.Text == "" || textBox21.Text == "" || textBox22.Text == "" || textBox23.Text == "")
            {
                MessageBox.Show("You must fill all the fields!");
                return false;
            }
            int n = 0;
            if(!int.TryParse(textBox21.Text,out n))
            {
                MessageBox.Show("Max user must to be number!");
                textBox21.Focus();
                return false;
            }
            if (!int.TryParse(textBox22.Text, out n))
            {
                MessageBox.Show("Question count must to be number!");
                textBox22.Focus();
                return false;
            }
            if (!int.TryParse(textBox23.Text, out n))
            {
                MessageBox.Show("Answer Timeout must to be number!");
                textBox23.Focus();
                return false;
            }
            return true;
        }
    }
}
