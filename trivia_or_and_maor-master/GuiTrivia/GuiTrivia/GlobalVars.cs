﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuiTrivia
{
    class GlobalVars
    {
        public static bool isSignIn = false;
        public static string LoggedUser = "";
        public static Dictionary<String, String> rooms = new Dictionary<string, string>() {
            { "key1", "value1" }
        };
        
        public static int RoomId = 0;
    }
}
